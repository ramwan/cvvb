#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 10 2021
This is VB_intialization intial value getting using the Hybrid VB for Hierarchical LBA model.
@author: Gajan
"""

#load standard libraries 
import numpy as np
import scipy.io as sio
from scipy.stats import invwishart


#load other libraries 
from lib_lba import ValGrad,Model,PriorHyperParams,LearningRateADADELTA,LearningRateADAM,Output,InitialMCMC,VbSettings,Lambda1
from vech import vech
from Hybrid_VAFC import Hybrid_VAFC # without parralel processing
from likelihood_hybrid import likelihood_Hybrid
from sklearn.model_selection import KFold
import copy
import time, pickle
import os

#added on August 24
from ModelSpecification import model_specification
from vb_adaptive_initialization import vb_adaptive_initialization



#load data set

if __name__=='__main__':
    #step0 data preparation

    #exp_name='My_experiment'
    exp_name = 'Forstmann'
    dataset_name='Forstmann.mat'
    dataset_path=os.path.join(dataset_name)

    K =5 # no of folds

    #load one model initial
    #intital_path=os.path.join('initial_MCMC.mat')
    # initial_MCMC= sio.loadmat(intital_path)

    # load 27 models initial
    intital_path_models=os.path.join('intial_values_forstmann.mat')
    # dmat= sio.loadmat(intital_path_models)
    # initial_MCMC_models=dmat['initial_MCMC_Forstmann']
    # temp1=initial_MCMC_models[1][0]

    mat = sio.loadmat(dataset_path)  # load mat-file
    mdata = mat['data']  # variable in mat file
    mdtype = mdata.dtype  # dtypes of structures are "unsized objects"
    data = {n: mdata[n][0, 0] for n in mdtype.names}

    #VB Tuning parameters
    max_iter = 200  # number of iterations
    max_norm = 1000  # upper bound the the norm of the gradients

    r = 1  # B has size dxr
    I = 10  # number of Monte Carlo samples to estimate the gradients
    window = 100
    patience_parameter = 20  #
    a_t = 0.9  # temperature for annealing

    learning_rate = LearningRateADADELTA(0.95, 10 ** (-7))

    silent = True# False #display estimates for at each iteration
    generate_samples = False

    #
    lambda1 = Lambda1(np.array([0]) , np.array([0]) ,np.array([0]))

    vb_settings=VbSettings(r, max_iter, max_norm, I, window, patience_parameter, silent, generate_samples, lambda1,learning_rate)


    if exp_name == 'My_experiment':
        model_index = np.array([3, 1, 2, 0, 1])
        model_name = np.array([3, 1, 1, 0, 1])
        D_latent = np.sum(model_index)
        prior_par_mu = np.zeros([D_latent, 1])
        prior_par_cov = np.eye(D_latent)
        prior_par_v_a = 2
        prior_par_A_d = np.ones([D_latent, 1])

        J=19
        M=1
        p = D_latent*(J+2) # dim(theta) = p

        prior_par = PriorHyperParams(prior_par_mu, prior_par_cov, prior_par_v_a, prior_par_A_d)

        model1 = Model(model_name, model_index, J, p, prior_par)
        model= list()
        model.append(model1)

    else:
        M,model = model_specification('Forstmann')


    # step2 randomly partition the data in Kfolds
    J = len(data['RE'])
    kf = KFold(n_splits=K, random_state=0, shuffle=True)
    data_fields = list(data.keys())

    train_ind = list()
    test_ind = list()
    for j in range(J):
        RT = data['RT'][j][0]

        train_j = list() #save subject train index for K folds
        test_j = list()  #save subject train index for K folds

        #loop for K folds
        for train_index, test_index in kf.split(RT):
            train_j.append(train_index)
            test_j.append(test_index)

        train_ind.append(train_j)
        test_ind.append(test_j)
        del train_j
        del test_j



    #Step 3: Kfold CVVB algorithm
    elpd_cvvb = np.zeros([K,M]) #store all ELPD estimates
    vb_results = list() #store all VB results
    total_time=0 #count time

    initial_value=list()

    for m in range(M):  #for m in range(1,M):

        time1 = time.time()
        D_alpha = int(np.sum(model[m].index)) # Also same as D_latent,  number of random effects per participant
        p = D_alpha * (J + 2)  # dim(theta) = p

        #not needed in python
        # model{m}.num_subjects = J;
        # model{m}.matching_function_1 = matching_function_1;
        # model{m}.matching_function_2 = matching_function_2;
        model_name=(' c ~'+ str(model[m].name[0]) + '| A ~ '+ str(model[m].name[1]) + '| v ~ ' + str( model[m].name[2]) + '| s ~ '+ str(model[m].name[3]) +'| t0 ~ '+ str(model[m].name[4])  )
        print('Model '+ str(m+1)+ str(model_name))

        ## VB initialization (CMC MC part)
        r= vb_settings.r

        if np.sum(model[m].index) < 10:
            epsilon=1 #scale parameter of covariance matrix
            N_iter=100 # number of iterations in PMwG to initialize VB
        elif  np.sum(model[m].index) < 20:
            epsilon=0.6 #scale parameter of covariance matrix
            N_iter=150
        else:
            epsilon=0.3 #scale parameter of covariance matrix
            N_iter=150


        R = 10 # number of particles in PMwG
        initial_MCMC_alpha = np.random.randn(D_alpha,J)
        initial_MCMC_mu = np.random.randn(D_alpha,1) # the initial values for parameter \mu
        initial_MCMC_sig2 = invwishart.rvs(D_alpha + 10,np.eye(D_alpha)) # the initial values for \Sigma
        initial_MCMC_a_d = 1/np.random.gamma(1/2,1,(D_alpha,1))


        #load manually model1 starting value from matlab
        # initial_start_value_model1 = sio.loadmat('initial_model1.mat')
        # initial_MCMC_alpha =initial_start_value_model1['initial_model1'][0][0]
        # initial_MCMC_mu = initial_start_value_model1['initial_model1'][1][0]
        # initial_MCMC_sig2 = initial_start_value_model1['initial_model1'][2][0]
        # initial_MCMC_a_d = initial_start_value_model1['initial_model1'][3][0]


        initial_MCMC= InitialMCMC(initial_MCMC_alpha,initial_MCMC_mu,initial_MCMC_sig2,initial_MCMC_a_d)

        #CVVB for the current model
        k=0
        vb_results_model_m= list() #save VB results of model m

        time2 = time.time()

        (MCMC_initial, other) = vb_adaptive_initialization(model[m], data, initial_MCMC, N_iter, R, epsilon)
        initial = np.vstack([np.reshape(MCMC_initial[3], (D_alpha * J, 1)), MCMC_initial[0].reshape(-1, 1),
                             np.log(MCMC_initial[2]).reshape(-1, 1)])
        initial_value.append(initial)
        lambda_mu = initial.squeeze()
        print(['K fold CVVB estimate of ELPD = ' + str(np.mean(elpd_cvvb[:,m]))+ ', K fold run time ' + str((time2-time1)/60) + ' minutes'])

        total_time = total_time + (time2 - time1)
        #vb_results.append(vb_results_model_m)
        del vb_results_model_m

            #k = k+1 looping is done in the above if/else statement

   # start = time.time()
    f = open('output_cvvb_mat_initials_Sep10_intials', 'wb')
    pickle.dump([model,initial_value], f)
    #pickle.dump([model,prior_par,initial_MCMC['initial'],output], f)
    f.close()

    print(['total run time ' + str(total_time/60) +' minutes'])


