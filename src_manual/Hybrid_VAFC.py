#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is  Hybrid VB VAFC for Hierarchical LBA model.
@author: Gajan
"""

import numpy as np
#import pandas as pd
#from scipy.special import gamma
#import scipy.io as sio
from lib_lba import ValGrad, Model, PriorHyperParams, LearningRateADADELTA, LearningRateADAM, Output,VbSettings, Lambda1
from scipy.stats import invwishart

from q_VAFC import q_VAFC
from prior_density_hybrid import prior_density_hybrid
# from likelihood_forstmann_hybrid import likelihood_Forstmann_Hybrid
from likelihood_forstmann_hybridv2 import likelihood_Forstmann_Hybrid
#from likelihood_forstmann_hybridv2_vec import likelihood_Forstmann_Hybrid_vec
from likelihood_hybrid import likelihood_Hybrid
from p_Sigma_alpha import p_Sigma_alpha
from vech import vech
import time
#from jax import grad, value_and_grad, jit


##load data
# mat = sio.loadmat('Forstmann.mat')  # load mat-file
# mdata = mat['data']  # variable in mat file
# mdtype = mdata.dtype  # dtypes of structures are "unsized objects"
# data = {n: mdata[n][0, 0] for n in mdtype.names}

# %%
# import numpy as npj (when needed)
# import jax.numpy as npj
# import jax
# from jax import grad,value_and_grad,jit
# from jax.scipy.stats import norm
# import time;

# model_name=np.array([3,1,1,0,1])
# model_index=np.array([3,1,2,0,1])
# #start= time.time()
# J=data['RT'].shape[0] #no of participants

# num_choice = 2; # the number of choice
# D_latent = np.sum(model_index); # number of random effects per participant
# D_G = D_latent + D_latent*(D_latent+1)/2 + D_latent # D_G = dim(theta_G)total number of global parameters
# p = D_latent*(J+2); # dim(theta) = p;

# model=Model(model_name,model_index,J,p)


# prior_par_mu= np.zeros([D_latent,1])
# prior_par_cov = np.eye(D_latent)
# prior_par_v_a = 2
# prior_par_A_d = np.ones([D_latent,1])

# prior_par= PriorHyperParams(prior_par_mu,prior_par_cov,prior_par_v_a,prior_par_A_d)


# max_iter = 5000 #number of iterations
# max_norm = 1000 # upper bound the the norm of the gradients

# r = 10 # B has size dxr
# I = 10 # number of Monte Carlo samples to estimate the gradients
# window = 200
# patience_parameter = 100#
# a_t = 0.9# temperature for annealing

# learning_rate=LearningRateADADELTA(0.95, 10**(-7))

# silent= False

# lambda_mu = initial #squeeze as a vector (147,)
# lambda_B = np.zeros([p,r])/r
# #lambda_B = tril(lambda.B)
# lambda_d = 0.01*np.ones([p,1])

# lambda1=Lambda1(lambda_mu , lambda_B , lambda_d)


# %%

# load data ()

# use lambda1 instead of lambda

def Hybrid_VAFC(model, data, vb_settings):

    #Added this on Aug 25 for generalized version
    lambda1 = vb_settings.initial
    I = vb_settings.I
    r = vb_settings.r
    max_iter = vb_settings.max_iter
    max_norm = vb_settings.max_norm
    patience_parameter= vb_settings.patience_parameter
    window =vb_settings.window
    silent=vb_settings.silent

    prior_par=model.prior_par

    learning_rate=vb_settings.learning_rate

    data_RE = data['RE']  # data_RE_11=data_RE[j-1][0] to access the J=1
    data_RT = data['RT']
    data_E = data['E']

    LB = np.zeros([max_iter, 1])

    J = len(data_RT)
    D_alpha = int(np.sum(model.index))
    p = D_alpha * (J + 2)
    v_a = prior_par.v_a  # check
    df = v_a + D_alpha + J - 1  # check this again(what is this?)
    df = np.double(df)  # make sure df is a scalar

    skip_count = 0  # count for gradients skips due to inf/nan

    # choose the optimizer
    if isinstance(learning_rate, LearningRateADADELTA):
        v = learning_rate.v
        eps = learning_rate.eps  # hyperparameters
        E_g2 = np.zeros([p * (r + 2), 1])
        E_delta2 = np.zeros([p * (r + 2), 1])

    elif isinstance(learning_rate, LearningRateADAM):
        beta_1 = learning_rate.beta_1
        beta_2 = learning_rate.beta_2
        eps = learning_rate.eps
        alpha = learning_rate.stepsize

        T = learning_rate.T  # after T, the stepsize is reduced
        m_t = np.zeros([p * (r + 2), 1])
        v_t = np.zeros([p * (r + 2), 1])

    else:
        print('Not a correct optimizer, check ')

    # 1st stage

    for t in range(0, window):
        mu = lambda1.mu
        mu = mu.squeeze()
        # mu=mu.reshape(-1,1) #added to make 1d array

        B = lambda1.B
        d = lambda1.d

        # if using rqmc
        # rqmc = normrnd_qmc(I,p+r);
        # epsilon = rqmc(:,1:p)';
        # z = rqmc(:,p+1:end)';

        # intiaalize grad arrays
        gradmu = np.zeros([p, I])
        gradB = np.zeros([p, r, I])
        gradd = np.zeros([p, I])

        LBs = np.zeros([I, 1])
        #start= time.time()
        for i in range(0, I):  # paralll for loop

            #start= time.time()
            # (1) Generate theta
            z = np.random.multivariate_normal(np.zeros([1, r]).flatten(), np.eye(r), 1).T
            epsilon = np.random.multivariate_normal(np.zeros([1, p]).flatten(), np.eye(p), 1).T

            theta_1 = (mu.reshape(-1,
                                  1) + B @ z + d * epsilon).squeeze()  # theta_1 = (alpha_1,...,alpha_J,mu_alpha,log a_1,...,log a_D)
            alpha = np.reshape(theta_1[:D_alpha * J], [D_alpha, J], order='F')
            mu_alpha = theta_1[D_alpha * J:D_alpha * (J + 1)]
            log_a = theta_1[D_alpha * (J + 1):]
            a = np.exp(log_a)

            Psi = 2 * v_a * np.diag(1 / a)

            for j in range(0, J):
                Psi = Psi + (alpha[:, j] - mu_alpha).reshape(-1, 1) @ (alpha[:, j] - mu_alpha).reshape(-1, 1).T

            Sigma_alpha = invwishart.rvs(df, Psi)
            # (2) Calculate the likelihood, prior and q_vb

           # start= time.time()
            # likelihood with matching function 1 and 2
            like = likelihood_Hybrid(model, alpha, data, True)   #58.3ms /22ms

            #old version of likelihood function specific
            #like = likelihood_Forstmann_Hybrid(model, alpha, data, True)  # man 44ms last argument = "true" means compute the gradient
                                                                         # man_single=27ms /parralel 11ms
            # prior = prior_density(model,ALPHA,mu_alpha,a,Sigma_alpha,prior_par)
            #time1=time.time()
            #print(f"Runtime of the program 176 is {time1 - start}")
            prior = prior_density_hybrid(model, alpha, mu_alpha, log_a, Sigma_alpha, prior_par)  # man 3.4ms
            # like = prior

            q_lambda = q_VAFC(theta_1, mu, B, d)  #

            # q_Sigma = p_Sigma_alpha(Sigma_alpha,df,v_a,Psi,ALPHA,mu_alpha,a)
            # start= time.time()
            q_Sigma = p_Sigma_alpha(Sigma_alpha, df, v_a, Psi, alpha, mu_alpha, log_a)  #
            #time1=time.time()
            #print(f"Runtime of the program is {time1 - start}")

            # (3) Estimate the lower bound
            LBs[i] = like.val + prior.val - q_lambda.val - q_Sigma.val
            # (4) Cumpute the gradients
            grad_theta_1_LB = like.grad + prior.grad - q_lambda.grad - q_Sigma.grad

            temp = grad_theta_1_LB
            gradmu[:, i] = temp.flatten()
            gradB[:, :, i] = temp @ z.T
            gradd[:, i] = (temp * epsilon).flatten()

        #time1=time.time()
        #print(f"Runtime of the program is {t,time1 - start}")

        # Estimate the Lower Bound
        LB[t] = np.mean(LBs)
        if (silent == False) & (np.mod(t + 1, 100) == 0):
            print(['iteration ', str(t), ' || LB: ', str(LB[t]), ' || standard error: ', str(np.std(LBs))])

        # Estimate the gradients
        grad_mu = np.mean(gradmu, 1)
        grad_B = np.mean(gradB, 2)
        grad_D = np.mean(gradd, 1)

        g = np.vstack([grad_mu.reshape(-1, 1), grad_B.flatten(order='F').reshape(-1, 1),
                       grad_D.reshape(-1, 1)])  # Stack gradient of LB into 1 column

        # check for nan/inf value in inf
        if (np.sum(np.isnan(g) + np.isinf(g)) > 0):
            if t == 0:
                # LB[t] = LB[0] #assign, t=0
                LB[t] = np.mean(LBs[np.isfinite(LBs)])  # get LBs mean for finite values
            else:
                LB[t] = LB[t - 1]  # assign previous value, t>0

            lambda1 = lambda1  # optional line
            skip_count = skip_count + 1

        else:
            # Gradient clipping
            norm_g = np.linalg.norm(g)
            if norm_g > max_norm:
                g = max_norm * g / norm_g

            # Update learning rate
            if isinstance(learning_rate, LearningRateADADELTA):
                E_g2 = v * E_g2 + (1 - v) * (g ** 2);
                rho = np.sqrt(E_delta2 + eps) / np.sqrt(E_g2 + eps)
                Delta = rho * g;
                E_delta2 = v * E_delta2 + (1 - v) * (Delta ** 2)

            elif isinstance(learning_rate, LearningRateADAM):
                m_t = beta_1 * m_t + (1 - beta_1) * g
                v_t = beta_2 * v_t + (1 - beta_2) * (g ** 2)

                Delta = alpha * np.sqrt(1 - beta_2 ** t) / (1 - beta_1 ** t) * m_t / (
                            np.sqrt(v_t) + eps * np.sqrt(1 - beta_2 ** t))

            # Update Lambda

            vec_lambda = np.vstack(
                [lambda1.mu.reshape(-1, 1), lambda1.B.flatten(order='F').reshape(-1, 1), lambda1.d.reshape(-1, 1)])
            vec_lambda = vec_lambda + Delta
            lambda1.mu = vec_lambda[:p]
            lambda1.B = np.reshape(vec_lambda[p:(p * (r + 1))], (p, r), order='F')
            lambda1.B = np.tril(lambda1.B)  # IMPORTANT: Upper Triangle of B must be zero !!!
            lambda1.d = vec_lambda[(p * (r + 1)):]

    # 2nd stage

    LB_smooth = np.zeros([max_iter - window + 1, 1])
    LB_smooth[0] = np.mean(LB[:t + 1])
    patience = 0

    lambda_best = lambda1
    max_best = LB_smooth[0]

    stopping_rule = False
    converge = 'reach_max_iter'
    t = t + 1
    t_smooth = 1  # iteration index for LB_smooth

    while (t <= (max_iter - 1)) & (stopping_rule == False):

        mu = lambda1.mu
        mu = mu.squeeze()
        # mu=mu.reshape(-1,1) #added to make 1d array

        B = lambda1.B
        d = lambda1.d

        # if using rqmc
        # rqmc = normrnd_qmc(I,p+r);
        # epsilon = rqmc(:,1:p)';
        # z = rqmc(:,p+1:end)';

        # intiaalize grad arrays
        gradmu = np.zeros([p, I])
        gradB = np.zeros([p, r, I])
        gradd = np.zeros([p, I])

        LBs = np.zeros([I, 1])

        for i in range(0, I):  ##parellel for loop

            # (1) Generate theta
            z = np.random.multivariate_normal(np.zeros([1, r]).flatten(), np.eye(r), 1).T
            epsilon = np.random.multivariate_normal(np.zeros([1, p]).flatten(), np.eye(p), 1).T

            theta_1 = (mu.reshape(-1,
                                  1) + B @ z + d * epsilon).squeeze()  # theta_1 = (alpha_1,...,alpha_J,mu_alpha,log a_1,...,log a_D)
            alpha = np.reshape(theta_1[:D_alpha * J], [D_alpha, J], order='F')
            mu_alpha = theta_1[D_alpha * J:D_alpha * (J + 1)]
            log_a = theta_1[D_alpha * (J + 1):]
            a = np.exp(log_a)

            Psi = 2 * v_a * np.diag(1 / a)

            for j in range(0, J):
                Psi = Psi + (alpha[:, j] - mu_alpha).reshape(-1, 1) @ (alpha[:, j] - mu_alpha).reshape(-1, 1).T

            Sigma_alpha = invwishart.rvs(df, Psi)
            # (2) Calculate the likelihood, prior and q_vb
#            like = likelihood_Forstmann_Hybrid(model, alpha, data,True)  # last argument = "true" means compute the gradient
            like = likelihood_Hybrid(model, alpha, data,
                                               True)
            # prior = prior_density(model,ALPHA,mu_alpha,a,Sigma_alpha,prior_par)

            prior = prior_density_hybrid(model, alpha, mu_alpha, log_a, Sigma_alpha, prior_par)

            q_lambda = q_VAFC(theta_1, mu, B, d)
            # q_Sigma = p_Sigma_alpha(Sigma_alpha,df,v_a,Psi,ALPHA,mu_alpha,a)

            q_Sigma = p_Sigma_alpha(Sigma_alpha, df, v_a, Psi, alpha, mu_alpha, log_a)

            # like=prior

            # (3) Estimate the lower bound
            LBs[i] = like.val + prior.val - q_lambda.val - q_Sigma.val
            # (4) Cumpute the gradients
            grad_theta_1_LB = like.grad + prior.grad - q_lambda.grad - q_Sigma.grad

            temp = grad_theta_1_LB
            gradmu[:, i] = temp.flatten()
            gradB[:, :, i] = temp @ z.T
            gradd[:, i] = (temp * epsilon).flatten()

        # Estimate the Lower Bound
        LB[t] = np.mean(LBs)
        LB_smooth[t_smooth] = np.mean(LB[t + 1 - window:t + 1])


        # Estimate the gradients
        grad_mu = np.mean(gradmu, 1)
        grad_B = np.mean(gradB, 2)
        grad_D = np.mean(gradd, 1)

        g = np.vstack([grad_mu.reshape(-1, 1), grad_B.flatten(order='F').reshape(-1, 1),
                       grad_D.reshape(-1, 1)])  # Stack gradient of LB into 1 column

        # Check for nan/inf
        if (np.sum(np.isnan(g) + np.isinf(g)) > 0):
            LB[t] = LB[t - 1]  # assign previous lambda, t>0
            LB_smooth[t_smooth] = np.mean(LB[t + 1 - window:t + 1])
            lambda1 = lambda1  # optional line
            skip_count = skip_count + 1

        else:

            # Gradient clipping
            norm_g = np.linalg.norm(g)
            if norm_g > max_norm:
                g = max_norm * g / norm_g

            # Update learning rate
            if isinstance(learning_rate, LearningRateADADELTA):
                E_g2 = v * E_g2 + (1 - v) * (g ** 2);
                rho = np.sqrt(E_delta2 + eps) / np.sqrt(E_g2 + eps)
                Delta = rho * g
                E_delta2 = v * E_delta2 + (1 - v) * (Delta ** 2)

            elif isinstance(learning_rate, LearningRateADAM):
                if t > T:
                    alpha = alpha * T / t

                m_t = beta_1 * m_t + (1 - beta_1) * g
                v_t = beta_2 * v_t + (1 - beta_2) * (g ** 2)

                Delta = alpha * np.sqrt(1 - beta_2 ** t) / (1 - beta_1 ** t) * m_t / (
                            np.sqrt(v_t) + eps * np.sqrt(1 - beta_2 ** t))

                # Update Lambda

            vec_lambda = np.vstack([lambda1.mu.reshape(-1, 1), lambda1.B.flatten(order='F').reshape(-1, 1), lambda1.d.reshape(-1, 1)])
            vec_lambda = vec_lambda + Delta
            lambda1.mu = vec_lambda[:p]
            lambda1.B = np.reshape(vec_lambda[p:(p * (r + 1))], (p, r), order='F')
            lambda1.B = np.tril(lambda1.B)  # IMPORTANT: Upper Triangle of B must be zero !!!
            lambda1.d = vec_lambda[(p * (r + 1)):]


        if (silent == False) & (np.mod(t + 1, 100) == 0):
            print(['iteration ', str(t), ' || smooth LB: ', str(LB_smooth[t_smooth]), ' || standard error: ',
                   str(np.std(LBs))])

        # Stopping Rule:
        if (LB_smooth[t_smooth] <= max_best) | (np.abs(LB_smooth[t_smooth] - LB_smooth[t_smooth - 1]) < 0.00001):
            patience = patience + 1

        else:
            patience = 0
            lambda_best = lambda1
            max_best = LB_smooth[t_smooth]

        # if (patience >patience_parameter):
        #     stopping_rule = True
        #     if max_best < -10 ** (5):
        #         print('Warning: Failed to converge, LB less than -10^5')
        #         converge = False
        #         break

        if (patience > patience_parameter):
            stopping_rule = True

            if(np.std(LB_smooth[t_smooth - patience_parameter+1:t_smooth]) > 10):
                print( ['  Warning: VB might not converge to a good local mode'
                        + 'Initial LB = ' + str(np.round(LB[0],1)) + ' || max LB = ' + str( np.round(max_best,1))])
                #converge = False
                converge = True
            else:
                converge = True

        t = t + 1
        t_smooth = t_smooth + 1


    if vb_settings.generate_samples == True:
        ## SAVE THE OUTPUT

        lambda1 = lambda_best
        mu = lambda1.mu
        B = lambda1.B
        d = lambda1.d

        v_a = prior_par.v_a
        df = v_a + D_alpha + J - 1

        N = 10000 #posterior samples
        theta_VB = np.zeros([int(p+D_alpha*(D_alpha+1)/2),N])

        for i in range (0,N):
            epsilon = np.random.randn(p,1)
            z = np.random.randn(r,1)

            theta_1 = (mu.reshape(-1,1) + B @z + d.reshape(-1,1)*epsilon).squeeze() # theta_1 = (alpha_1,...,alpha_J,mu_alpha,log a_1,...,log a_D)
            alpha = np.reshape(theta_1[:D_alpha*J],[D_alpha,J],order='F')
            mu_alpha = theta_1[D_alpha*J :D_alpha*(J+1)]
            log_a = theta_1[D_alpha*(J+1):]
            a = np.exp(log_a)

            Psi = 2*v_a*np.diag(1/a)

            for j in range (0,J):
                Psi = Psi + (alpha[:,j]-mu_alpha).reshape(-1,1) @ (alpha[:,j]-mu_alpha).reshape(-1,1).T

            Sigma_alpha = invwishart.rvs(df,Psi)

            C = np.linalg.cholesky(Sigma_alpha)
            C_star = np.copy(C)
            np.fill_diagonal(C_star, np.log(np.diag(C)))

            theta_VB[:,i] = np.hstack([alpha.flatten(order='F'), mu_alpha, vech(C_star).squeeze(), log_a])

        return Output(lambda_best, LB[:t - 1], LB_smooth[:t_smooth - 1], max_best, skip_count, converge,theta_VB)

    #output as a object
    else:
        return Output(lambda_best, LB[:t - 1], LB_smooth[:t_smooth - 1], max_best, skip_count, converge,np.array([0]))

# test here
# Hybrid_VAFC(model,data,prior_par,lambda1,r,I,max_iter,window,patience_parameter,learning_rate,max_norm,silent)
