# -*- coding: utf-8 -*-
"""
Created on Mon May 31 7:14:57 2021
This function does the prior density in the hybrid LBA method 
"""

import numpy
#import numpy as npj
import numpy as npj
#import jax
import pandas as pd
from numpy.linalg import slogdet
from multigamma import multigamma
from inv_gamma_pdf import inv_gamma_pdf
#from jax import grad, value_and_grad,jit
from lib_lba import ValGrad,Model,PriorHyperParams


## This function is for derived log prior
#def obtain_log_prior_VAFC_LBA(model,theta, prior_hyper_params):
#def obtain_log_prior_VAFC_LBA(model,alpha,mu_alpha,a,Sigma_alpha,prior_par): 
#had to change this to loga    
def obtain_log_prior_VAFC_LBA(D_alpha,J,alpha,mu_alpha,log_a,Sigma_alpha,prior_par_v_a,prior_par_A_d):     
    

        
    #D_alpha= npj.sum(model.index)# random affects /per participants
    #J=model.num_subjects # no of participants
    #D_alpha=npj.array(D_alpha)
    #D_alpha = 7
    D_alpha=alpha.shape[0]
    J=npj.array(J)
#    prior_par_v_a=npj.array(prior_par_v_a)
#    prior_par_A_d=npj.array(prior_par_A_d)
    
    
    v = prior_par_v_a+ D_alpha - 1

    a = npj.exp(log_a) 
    

    Sigma_alpha_inv=npj.linalg.inv(Sigma_alpha)

    # #A = Sigma_alpha_inv*(alpha - mu_alpha) # this method to avoid numerical error 
    A = Sigma_alpha_inv @(alpha-(npj.array([mu_alpha.flatten() ]*alpha.shape[1]).T))

    #log prior forumulation 
    log_Jacobian = npj.sum(npj.log(a)) #log Jacobian of log_a_d 
    
    
    log_prior=  -0.5*D_alpha*(J+1)*npj.log(2*npj.pi) - 0.5*(J + v + D_alpha +1)*slogdet(Sigma_alpha)[1]\
                          -0.5*npj.trace((alpha-(npj.array([mu_alpha.flatten() ]*alpha.shape[1]).T)).T @ A) - 0.5*(mu_alpha.T @ mu_alpha) \
                              + 0.5* v * (D_alpha* npj.log(2*prior_par_v_a) - npj.sum(npj.log(a) )) - 0.5*v*D_alpha*npj.log(2) \
                                  - npj.log(multigamma(v/2,D_alpha)) - prior_par_v_a * npj.sum(npj.diag(Sigma_alpha_inv)/a) \
                                      + npj.sum(inv_gamma_pdf( a, 0.5*npj.ones([D_alpha,1]), 1 /(prior_par_A_d**2)))+ log_Jacobian

    # log_prior=  -0.5*D_alpha*(J+1)*npj.log(2*npj.pi) - 0.5*(J + v + D_alpha +1)*slogdet(Sigma_alpha)[1]\
    #                        -0.5*npj.trace((alpha-(npj.array([mu_alpha.flatten() ]*alpha.shape[1]).T)).T @ A) - 0.5*(mu_alpha.T @ mu_alpha) \
    #                            + 0.5* v * (D_alpha* npj.log(2*prior_par_v_a) - npj.sum(npj.log(a) )) - 0.5*v*D_alpha*npj.log(2) \
    #                                - npj.log(jit(multigamma)(v/2,D_alpha)) - prior_par_v_a * npj.sum(npj.diag(Sigma_alpha_inv)/a) \
    #                                    + npj.sum(jit(inv_gamma_pdf)( a, 0.5*npj.ones([D_alpha,1]), 1 /(prior_par_A_d**2)))+ log_Jacobian

    #gradients wrt to alpha1, ...
    grad_alpha  = -A.flatten(order='F')

    # gradients wrt to mu_alpha
    grad_mu = npj.sum(A, 1) - mu_alpha

    #gradient wrt log(a)
    grad_loga = -0.5 * v + 2 * (npj.diag(Sigma_alpha_inv) / a) - 0.5 + 1/ a

    prior_grad=npj.vstack((grad_alpha.reshape(-1,1),grad_mu.reshape(-1,1),grad_loga.reshape(-1,1)))
    return (log_prior,prior_grad)

## This is for AD posterior gradients
# def obtain_log_prior_VAFC_LBA_AD(model,theta, prior_hyper_params):
#      theta_grad=grad(obtain_log_prior_VAFC_LBA,1)(model,theta, prior_hyper_params) # 2 for grad theta
#      return theta_grad
 
def prior_density_hybrid(model,alpha,mu_alpha,log_a,Sigma_alpha,prior_par):
     #log=obtain_log_prior_VAFC_LBA(model,alpha,mu_alpha,log_a,Sigma_alpha,prior_par)
     D_alpha= npj.sum(model.index)# random affects /per participants
     J=model.num_subjects # no of participants
     
     prior_par_v_a=npj.array(prior_par.v_a)
     prior_par_A_d=prior_par.A_d
     
     #theta_grad=value_and_grad(obtain_log_prior_VAFC_LBA,(2,3,4))(D_alpha,J,alpha,mu_alpha,log_a,Sigma_alpha,prior_par_v_a,prior_par_A_d) # 2 for grad theta
     theta_grad = obtain_log_prior_VAFC_LBA(D_alpha, J, alpha, mu_alpha, log_a, Sigma_alpha,
                                                                       prior_par_v_a, prior_par_A_d)  # 2 for grad theta

     #log_val=theta_grad[0]
    # grad_val= npj.vstack([theta_grad[1][0].flatten(order='F').reshape(-1,1),theta_grad[1][1].reshape(-1,1),theta_grad[1][2].reshape(-1,1)]) # merge alpha, mu,log_a gradient
     return ValGrad(theta_grad[0], theta_grad[1])   #convert to numpy array


# class PriorHyperParams:
#     def __init__(self, mu, cov, v_a, A_d):
#         self.mu = mu
#         self.cov = cov
#         self.v_a = v_a
#         self.A_d = A_d


# class Model:
#     def __init__(self, name, index, num_subjects, dim):
#         self.name = name
#         self.index = index
#         self.num_subjects = num_subjects
#         self.dim = dim
        
        
# class ValGrad:
#     def __init__(self, val, grad):
#         self.val  = val
#         self.grad = grad  # array (n,1)

     
#define model parameters
# model_index=npj.array([3, 1, 2, 0, 1])
# model_name=npj.array([3, 1, 1, 0, 1])
# D_latent = npj.sum(model_index)
#
# model= Model(model_name,model_index, 19,147)
#
#
# #define hyper parameters
# prior_par_mu= npj.zeros([D_latent,1])
# prior_par_cov = npj.eye(D_latent)
# prior_par_v_a = 2
# prior_par_A_d = npj.ones([D_latent,1])
#
# prior_par= PriorHyperParams(prior_par_mu,prior_par_cov,prior_par_v_a,prior_par_A_d)


#test
#log_a=npj.log(a); # This is important here
#output_log=prior_density_hybrid(model,alpha,mu_alpha,log_a,Sigma_alpha,prior_par)
#output=obtain_log_prior_VAFC_LBA_AD2(model,alpha,mu_alpha,log_a,Sigma_alpha,prior_par)
