import numpy as np
from scipy.stats import invwishart
from likelihood_hybrid import matching_forstmann
from LBA_pdf_v2 import LBA_pdf_v2
from vech import vech
from scipy.stats import multivariate_normal

def vb_adaptive_initialization(model,data,initial_MCMC,N_iter,R,epsilon):
## DESCRIPTION:
# This function is used to carefully choose an initial value for VB (by
# running PMwG with a small number of iterations (R = 50 to 100).

# INPUT: model = model structure
#        data = the data
#        initial_MCMC = initial values for PMwG
#        N_iter = total number of iterations.
#        R = number of particles
#        epsilon = scale parameter
# OUTPUT: .ouput = average of the latest MCMC draws (the first half of MCMC draws are thrown away as burn-in) ;
#         .initial = store the last MCMC draw


    J = int(model.num_subjects ) # number of participants
    D_alpha = int(np.sum(model.index)) # number of random effects per participant
    #Matching_Function_1 = str2func(model.matching_function_1) ##

    ## variables to store MCMC values ###
    mu_store = np.zeros([D_alpha,N_iter])
    vech_C_star_store = np.zeros([int(D_alpha*(D_alpha+1)/2),N_iter]) #C^* is tranformed !
    a_d_store = np.zeros([D_alpha,N_iter])
    alpha_store = np.zeros([D_alpha,J,N_iter]) # 3-D matrix that stores random effects draws

    ##  Initialization ##
    alpha = initial_MCMC.alpha
    theta_G_mu = initial_MCMC.mu #the initial values for parameter \mu
    theta_G_sig2 = initial_MCMC.sig2 # the initial values for \Sigma
    theta_G_a_d = initial_MCMC.a_d

    # THE MCMC Particles Metropolis within Gibbs
    t = 0
    while t < N_iter:

        #Sample \mu|rest in Gibbs step
        var_mu = np.linalg.inv(J * np.linalg.inv(theta_G_sig2) + np.linalg.inv(model.prior_par.cov)) @ np.eye(D_alpha) # use @ here # General formula would be: var_mu = inv(J/theta.sig2 + inv(prior_mu_sig2));
        mean_mu = var_mu @ ( (np.linalg.inv(theta_G_sig2) @  np.sum(alpha,1).reshape(-1,1) ) + np.linalg.inv(model.prior_par.cov) @ model.prior_par.mu)
        #mean_mu=0
        theta_G_mu = np.random.multivariate_normal(mean_mu.squeeze(),var_mu,1).T


        # Sample \Sigma|rest in Gibbs step
        k_a = model.prior_par.v_a + D_alpha - 1 + J
        cov_temp = np.zeros([D_alpha,D_alpha])
        for j in range(J):
            cov_temp = cov_temp + (alpha[:, j].reshape(-1,1) - theta_G_mu) @ (alpha[:, j].reshape(-1,1) - theta_G_mu).T

        B_a = 2 * model.prior_par.v_a * np.diagflat(1 / theta_G_a_d) + cov_temp
        theta_G_sig2 = invwishart.rvs(k_a, B_a)#iwishrnd(B_a, k_a)
        theta_sig2_inv = np.linalg.inv(theta_G_sig2)

        #Sample a_{1},...,a_{7}|rest in Gibbs step
        theta_G_a_d = 1 / np.random.gamma((model.prior_par.v_a + D_alpha) / 2, 1 / (model.prior_par.v_a * np.diag(theta_sig2_inv).reshape(-1,1) + (1 / model.prior_par.A_d) ** 2))


        #parfor possible here
        for j in range(J):

            n_j = len(data['RT'][j][0])

            #(step 1): Generate alpha_j from the proposal distribution

            alpha_j_k = alpha[:, j].reshape(-1,1) #% the set of random effects from previous iteration of MCMC for conditioning.
            w_mix = 0.5 #% setting the weights of the mixture in the burn in and initial sampling stage.
            u = np.random.rand(R,1)
            id1 = (u<w_mix)
            n1 = np.sum(id1)
            n2 = R-n1
            chol_covmat = np.linalg.cholesky(theta_G_sig2)
            rnorm1 = alpha_j_k + epsilon *chol_covmat @ np.random.randn(D_alpha,n1)
            rnorm2 = theta_G_mu + chol_covmat @ np.random.randn(D_alpha,n2)
            alpha_j_R = np.hstack([rnorm1, rnorm2]) # % alpha_j_R = [alpha_j^1, ... alpha_j^R]. Particles alpha_j^r are stores in colums of matrix alpha_j_R

            alpha_j_R[:,0] = alpha_j_k.squeeze()

            # (step 2): Compute the importance weights

            #Duplicate data (y_j) and stack them into a column vector

            RT_j = np.tile(data['RT'][j][0],(R,1))
            RE_j = np.tile(data['RE'][j][0],(R,1)) # RE = 1 (error) 2 (correct)

            #Match the random effects with the observations

            #kron product not efficient, hence a for loop matchin function and LBA_pdf call once (efficient for AD)
            alpha_j_R_subject=np.tile(alpha_j_R[:,0].reshape(-1,1),J) # tile as times subjects to follow
            m1_temp_particle = matching_forstmann(model, alpha_j_R_subject, data, j) # start for one step
            for ii in range(1, R): #go for next steps here 2 to R particles
                alpha_j_R_subject = np.tile(alpha_j_R[:, ii].reshape(-1, 1), J)
                m1_temp_ii = matching_forstmann(model,alpha_j_R_subject, data, j)# tile the J subject factor J times , assign J for which subject
                m1_temp_particle[0] = np.vstack([m1_temp_particle[0],m1_temp_ii[0]])
                m1_temp_particle[1] = np.vstack([m1_temp_particle[1], m1_temp_ii[1]])
                m1_temp_particle[2] = np.vstack([m1_temp_particle[2], m1_temp_ii[2]])
                m1_temp_particle[3] = np.vstack([m1_temp_particle[3], m1_temp_ii[3]])
                m1_temp_particle[4] = np.vstack([m1_temp_particle[4], m1_temp_ii[4]])
                del m1_temp_ii
            #print(j)



            LBA_j= LBA_pdf_v2(RE_j, RT_j, m1_temp_particle[0], m1_temp_particle[1], m1_temp_particle[2], m1_temp_particle[3], m1_temp_particle[4],False)
            del m1_temp_particle

            log_LBA_j = LBA_j[2]
            lw_reshape = np.reshape(log_LBA_j, (n_j, R), order='F')
            logw_first = np.sum(lw_reshape,0)

            #Computing the log of p(\alpha|\theta) and density of the proposal for

            logw_second = np.log(multivariate_normal.pdf(alpha_j_R.T,theta_G_mu.T.squeeze(), theta_G_sig2))
            logw_third = np.log(w_mix *multivariate_normal.pdf(alpha_j_R.T,alpha_j_k.squeeze(), (epsilon ** 2) * theta_G_sig2)+(1 - w_mix) * multivariate_normal.pdf(alpha_j_R.T,theta_G_mu.squeeze(), theta_G_sig2))
            logw = logw_first + logw_second - logw_third


            max_logw = np.max(logw)
            weight = np.exp(logw - max_logw)
            weight = weight / np.sum(weight)


            Nw = len(weight)

            if (np.sum(weight > 0) > 0):
                ind = np.random.choice(Nw, 1, replace=True,p=weight)
                #ind= ind-1
                alpha[:, j] = alpha_j_R[:, ind].squeeze()



        #storing the MCMC draws

        mu_store[:, t] = theta_G_mu.squeeze()
        chol_sig2 = np.linalg.cholesky(theta_G_sig2)
        C_star = chol_sig2
        C_star[0: D_alpha + 1:] = np.log(np.diag(chol_sig2))
        vech_C_star_store[:, t] = vech(C_star).squeeze()
        a_d_store[:, t] = theta_G_a_d.squeeze()
        alpha_store[:,:, t] = alpha

        t = t + 1

        #save the final output in the standard MCMC output structure


    N_burn = int(np.ceil(N_iter / 2)) # the burn - in iterations

    initial_alpha = alpha
    initial_mu = theta_G_mu
    initial_a_d = theta_G_a_d
    initial_sig2 = theta_G_sig2

    output_mu_store = np.mean(mu_store[:, N_burn: ], 1)
    output_vech_C_star_store = np.mean(vech_C_star_store[:, N_burn:], 1)
    output_a_d_store = np.mean(a_d_store[:, N_burn:], 1)
    output_alpha_store = np.mean(alpha_store[:,:, N_burn:], 2)

    output=(output_mu_store,output_vech_C_star_store,output_a_d_store,output_alpha_store  )
    return (output, (initial_alpha,initial_mu,initial_a_d,initial_sig2))



