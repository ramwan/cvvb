# -*- coding: utf-8 -*-
"""
Created on Fri May 20 13:13:42 2021

Can we include this into the automatic AD part??
Modified on Jun 8 included AD.
"""

#import numpy
#import jax.numpy as np
import numpy as np
#from jax import grad, value_and_grad,jit
from lib_lba import ValGrad


def q_VAFC(theta,mu,B,d):
    # NOTE: This version is the same as q_gaussian_factor_covariance.m, except
    # that the input is (mu,B,d), not lambda as in the latter
    # INPUT: lambda = (mu,B,d), theta ~ N(mu,covmat)
    #                 with covmat = lambda.B*lambda.B' + diag(lambda.d.^2);
    # OUTPUT: y_log = log (q_lambda)
    #         y_theta = inv(BB'+D^2)*(Bz + d.*eps) %gradient of q wrt beta

    [m, p] = B.shape
    covmat = B@B.T + np.diagflat(d**2)
    term = theta - mu
    D_inv2 = np.diagflat(d**(-2))

    precision_matrix = D_inv2 - D_inv2 @ B @np.linalg.inv(np.eye(p) + B.T @ D_inv2 @ B)@ B.T@ D_inv2

   # y_log = -0.5*p*np.log(2*pi) - 0.5*logdet(covmat) - 0.5*term.T/covmat*term
    y_log = -0.5*p*np.log(2*np.pi) - 0.5*np.linalg.slogdet(covmat)[1] - 0.5*term.T @ np.linalg.inv(covmat) @term

    y_grad = - precision_matrix @term

    return ValGrad(y_log,y_grad.reshape(-1,1))


# def q_VAFC_log(theta,mu,B,d):
#     # NOTE: This version is the same as q_gaussian_factor_covariance.m, except
#     # that the input is (mu,B,d), not lambda as in the latter
#     # INPUT: lambda = (mu,B,d), theta ~ N(mu,covmat)
#     #                 with covmat = lambda.B*lambda.B' + diag(lambda.d.^2);
#     # OUTPUT: y_log = log (q_lambda)
#     #         y_theta = inv(BB'+D^2)*(Bz + d.*eps) %gradient of q wrt beta
#
#     [m, p] = B.shape
#     covmat = B@B.T + np.diagflat(d**2)
#     term = theta - mu
#
#     y_log = -0.5*p*np.log(2*np.pi) - 0.5*np.linalg.slogdet(covmat)[1] - 0.5*term.T @ np.linalg.inv(covmat) @term
#
#
#     return np.reshape(y_log ,())
#
#
# def q_VAFC(theta,mu,B,d):
#      theta_grad=value_and_grad(jit(q_VAFC_log),(1))(theta,mu,B,d) # 2 for grad theta
#      log_val=theta_grad[0]
#      grad_val= -1*theta_grad[1].reshape(-1,1) # added to match manual gradient
#      return ValGrad(numpy.array(log_val), numpy.array(grad_val))


#test 
#output=q_VAFC(theta,mu,B,d)
