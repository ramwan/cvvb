import numpy as np
#from scipy.stats import norm
from scipy.stats import norm
def phi_prime_v2(x):
    return -norm.pdf(x) * x

