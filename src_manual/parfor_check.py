from time import sleep
import time
import numpy as np
from parfor import parfor
import multiprocessing
import concurrent.futures

def my_fun(*args, **kwargs): #0.909s
    @parfor(range(1000))
    def fun(i):
        #i=j
        sleep(0.00001)
        #print(i)
        return i*i**2
    return fun

def my_fun1(*args, **kwargs): #0.0002s

    fun = []
    for i in range(1000):
        #sleep(1)
        fun.append(i * i ** 2)
    #print(fun)
    return fun

def my_fun2(*args, **kwargs): #0.0003s

    fun = np.zeros([1,1000])
    for i in range(1000):
        #sleep(1)
        fun[0,i] = i * i ** 2
    #print(fun)
    return fun

# if __name__ == '__main__':
#     start = time.time()
#     #print(my_fun())
#     #print(my_fun1())
#     my_fun()
#     time1= time.time()
#
#     print(time1-start)

if __name__ == '__main__':

   # print(multiprocessing.cpu_count())

   with concurrent.futures.ProcessPoolExecutor(max_workers = 4) as executor:
       process_results = []
       for i in range(20):
           process_results.append(executor.submit(np.sum, [1, 2, 3, 4]))

   for i in process_results[0:30]:
       print(i.result())
