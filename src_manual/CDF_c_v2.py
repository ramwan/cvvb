#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri 28 9:21:12 2021
This function calculates the F_k(t) component.
The gradient is not calculated here as in MATLAB 
cdf_c(t,b,A,v,s,tau,gradient) is changed to cdf_c(t,b,A,v,s,tau) in python

"""

import jax.numpy as jnp
from jax.scipy.stats import norm
from phi_prime_v2 import phi_prime_v2
from jax import value_and_grad, vmap, jit
import time


def cdf_c_v2(t,b,A,v,s,tau, gradient):
    t_diff = t-tau

    #shortcut computation (see paper page 5 for notation x,y,z,u)
    w1 = (b - t_diff*v)/(s*t_diff)
    w2 = A/(s*t_diff)

    ##use jax.scipy.stats here, some difference is there between matlab /python
    x = norm.cdf(w1-w2) # (b - A - t_diff*v)/(s*t_diff)
    x1 = norm.pdf(w1-w2)
    z = norm.cdf(w1) # (b - t_diff*v)/(s*t_diff)
    z1 = norm.pdf(w1)
    
    u1 = (b-A-t_diff*v)/A     
    u2 = (b-t_diff*v)/A
    
    #to avoid numerical error (as in matlab code) comment this if using jax numpy (handled in hybrid VAFC)
    """
    id = x > 0.999
    x[id] = 0.999
    id = x <= 0.001
    x[id] = 0.001

    id = z > 0.999
    z[id] = 0.999
    id = z < 0.001
    z[id] = 0.001
    """

    F_k_t_sub= -u1*x + u2*z - x1/w2 + z1/w2

    #if (jnp.sum(jnp.isnan(F_k_t_sub)+ jnp.isinf(F_k_t_sub)) >0):
        #print(1)

    #to avoid numerical zero devision comment this if using jax numpy (handled in hybrid VAFC)
    """
    ind_fc = F_k_t_sub <= 10**(-50)
    F_k_t_sub[ind_fc] = 10**(-50)
    """

    F_k_t= 1-F_k_t_sub

    if gradient == True:
        x2 = phi_prime_v2(w1 - w2)
        z2 = phi_prime_v2(w1)
        w3 = ((b - A) / s) / (t_diff ** 2)

        grad_b = (1. / A)* x - z/ A + (u1* x1 - u2* z1 + (1/ w2)* (x2 - z2))/ (s* t_diff)
        grad_A = (-x + x1* A* u1/ (s* t_diff))/ A - (A * u1* x)/ A**2 + u2* z/ A + (
                    x2* w2 - x1 + z1)/ (s* t_diff* w2**2)
        grad_v = -t_diff* x/ A + (u1* x1 - z1* u2)/ s + t_diff* z/ A + (x2 - z2)/ (s* w2)

        w12_s = (t_diff* v - b + A)/ (t_diff* s**2) # derivative

        w1_s = (t_diff* v - b)/ (t_diff* s**2) # derivative
        w2_s = -A/ (t_diff* s**2)
        grad_s = u1* x1* w12_s - u2* z1* w1_s + ((x2* w12_s - z2* w1_s)* w2 - (x1 - z1)* w2_s)/ w2**2
        grad_tau = (v/A)*x + u1*x1*w3 - (v/A)*z - u2*z1*(b/(s*t_diff**2)) +(x2*w2*w3 - x1*A/(s*t_diff**2) )/w2**2 - ( z2*w2*b/(s*t_diff**2) - z1*(A/(s*t_diff**2)) )/w2**2

        output_grad_b = -grad_b/F_k_t_sub # NOTE: term = 1- F_k(t)
        output_grad_A = -grad_A/F_k_t_sub # NOTE: grads hare are of log ( 1 - F_k(t)) !!!
        output_grad_v = -grad_v/F_k_t_sub
        output_grad_s = -grad_s/F_k_t_sub
        output_grad_tau = -grad_tau/F_k_t_sub

        output_grad = jnp.hstack((output_grad_b, output_grad_A, output_grad_v, output_grad_s, output_grad_tau))
        #return (jnp.sum(jnp.log(jnp.array(1-F_k_t))),output_grad)  #method 2
        return (jnp.sum(jnp.log(jnp.array(F_k_t_sub))), output_grad)  #

    else:
        return (jnp.sum(jnp.log(jnp.array(F_k_t_sub))), jnp.array([0]), jnp.log(jnp.array(F_k_t_sub))) # additionally sent log element wise

def cdf_c_v2_no_grad(t,b,A,v,s,tau):
    t_diff = t-tau

    #shortcut computation (see paper page 5 for notation x,y,z,u)
    #w1 = (b - t_diff*v)/(s*t_diff)
    #w2 = A/(s*t_diff)
    
    w1 = lambda b1, t1, v1, s1, tau1: jnp.divide((b1 - jnp.multiply(t1 - tau1,v1)), jnp.multiply(s1,t1 - tau1))
    w2 = lambda A1, s1, t1, tau1: jnp.divide(A1, jnp.multiply(s1,t1 - tau1))

    ##use jax.scipy.stats here, some difference is there between matlab /python
    #x = norm.cdf(w1-w2) # (b - A - t_diff*v)/(s*t_diff)
    #x1 = norm.pdf(w1-w2)
    #z = norm.cdf(w1) # (b - t_diff*v)/(s*t_diff)
    #z1 = norm.pdf(w1)

    x = lambda b1, t1, v1, s1, A1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    x1 = lambda b1, t1, v1, s1, A1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    z = lambda b1, t1, v1, s1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1))
    z1 = lambda b1, t1, v1, s1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1))
    
    #u1 = (b-A-t_diff*v)/A     
    #u2 = (b-t_diff*v)/A

    u1 = lambda b1, A1, t1, v1, tau1: jnp.divide(b1-A1-jnp.multiply(t1 - tau1,v1), A1)
    u2 = lambda b1, t1, v1, A1, tau1: jnp.divide(b1-jnp.multiply(t1 - tau1,v1), A1)

    # we use sum() in order to turn a (1,) vector into a scalar
    F_k_t_sub = \
        lambda b1, A1, v1, s1, t1, tau1: \
               jnp.log(-jnp.multiply(u1(b1, A1, t1, v1, tau1), x(b1, t1, v1, s1, A1, tau1))\
                       +jnp.multiply(u2(b1, t1, v1, A1, tau1), z(b1, t1, v1, s1, tau1)) \
                       -jnp.divide(x1(b1, t1, v1, s1, A1, tau1), w2(A1, s1, t1, tau1)) \
                       +jnp.divide(z1(b1, t1, v1, s1, tau1), w2(A1, s1, t1, tau1)))\
    #F_k_t = lambda b1, A1, v1, s1, t1: jnp.log(1. - F_k_t_sub(b1, A1, v1, s1, t1))
    val = F_k_t_sub(b, A, v, s, t, tau)

    return (jnp.sum(val), jnp.array([0]), val) # additionally sent log element wise

#@jit
def cdf_c_v2_ad_grad(t,b,A,v,s,tau):
    t_diff = t-tau
    
    w1 = lambda b1, t1, v1, s1, tau1: jnp.divide((b1 - jnp.multiply(t1 - tau1,v1)), jnp.multiply(s1,t1 - tau1))
    w2 = lambda A1, s1, t1, tau1: jnp.divide(A1, jnp.multiply(s1,t1 - tau1))

    x = lambda b1, t1, v1, s1, A1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    x1 = lambda b1, t1, v1, s1, A1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    z = lambda b1, t1, v1, s1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1))
    z1 = lambda b1, t1, v1, s1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1))

    u1 = lambda b1, A1, t1, v1, tau1: jnp.divide(b1-A1-jnp.multiply(t1 - tau1,v1), A1)
    u2 = lambda b1, t1, v1, A1, tau1: jnp.divide(b1-jnp.multiply(t1 - tau1,v1), A1)

    # we use sum() in order to turn a (1,) vector into a scalar
    F_k_t_sub = \
        lambda b1, A1, v1, s1, t1, tau1: \
            jnp.sum(\
               jnp.log(-jnp.multiply(u1(b1, A1, t1, v1, tau1), x(b1, t1, v1, s1, A1, tau1))\
                       +jnp.multiply(u2(b1, t1, v1, A1, tau1), z(b1, t1, v1, s1, tau1)) \
                       -jnp.divide(x1(b1, t1, v1, s1, A1, tau1), w2(A1, s1, t1, tau1)) \
                       +jnp.divide(z1(b1, t1, v1, s1, tau1), w2(A1, s1, t1, tau1)))\
            )
    #F_k_t = lambda b1, A1, v1, s1, t1: jnp.log(1. - F_k_t_sub(b1, A1, v1, s1, t1))

    output_grad = value_and_grad(F_k_t_sub, (0, 1, 2, 3, 5))
    (val, grads) = vmap(output_grad)(b, A, v, s, t, tau) 
    val = jnp.sum(val)
    grads = jnp.hstack(grads)

    return (val, grads)

def diff_manual_ad(t, b, A, v, s, tau, gradient):
    if gradient:
        startm = time.perf_counter()
        vm, gm = cdf_c_v2(t, b, A, v, s, tau, gradient)
        endm = time.perf_counter()
        va, ga = cdf_c_v2_ad_grad(t, b, A, v, s, tau)
        enda = time.perf_counter()
        """
        t = jnp.array([[1., 1., 1.], [1., 1., 1.]])
        b = jnp.array([[1., 1., 1.], [1., 1., 1.]])
        A = jnp.array([[1., 1., 1.], [1., 1., 1.]])
        v = jnp.array([[1., 1., 1.], [1., 1., 1.]])
        s = jnp.array([[1., 1., 1.], [1., 1., 1.]])
        tau = jnp.array([[0.1, 0.1, 0.1], [0.1, 0.1, 0.1]])
        gradient = True
        va, ga = cdf_c_v2_ad_grad(t, b, A, v, s, tau)
        enda1 = time.perf_counter()
        t = jnp.array([[2., 1., 1.], [1., 1., 1.]])
        va, ga = cdf_c_v2_ad_grad(t, b, A, v, s, tau)
        enda2 = time.perf_counter()
        """

        print("value diff: ", jnp.abs(vm - va))
        print("gradient diff: ", jnp.abs(jnp.vdot(gm - ga, gm - ga)))
        print("manual time (ms): ", (endm-startm)*1000 )
        print("auto jit time (ms): ", (enda-endm)*1000 )
        #print("auto jit 2 time (ms): ", (enda1-enda)*1000 )
        #print("auto jit 3 time (ms): ", (enda2-enda1)*1000 )
        
        return (va, ga)
    else:
        return cdf_c_v2_ad_no_grad(t, b, A, v, s, tau)
