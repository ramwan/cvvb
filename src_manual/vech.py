#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu 27 9:44:23 2021
This is to generate vech(A) function

"""
import numpy as npj

#import time

def vech(A):
 
  d1,d2=A.shape
  
  # ind=npj.tril(npj.ones([d,d]))
  # output= A[ind==1]
  output= npj.zeros([npj.int(npj.tril(npj.ones([d1,d2])).sum()),1])
  
  
  i=0
  for c in range(0,A.shape[1]):
     for r in range (c,A.shape[0]):
        output[i,0]=A[r,c]
        i=i+1;
          
  
  return output


