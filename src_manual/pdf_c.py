#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26 17:36:12 2021
This function calculates the fc(t) component.
The gradient is not calculated here as in MATLAB 
pdf_c(t,b,A,v,s,tau,gradient) is changed to pdf_c(t,b,A,v,s,tau) in python

"""
# import numpy as npj
# from scipy.stats import norm


#jax enable
import jax.numpy as npj
from jax.scipy.stats import norm
from phi_prime import phi_prime
import jax

def pdf_c(t,b,A,v,s,tau):     
      
    t_diff = t-tau

    #shortcut computation (see paper page 5 for notation x,y,z,u)
    w1 = (b - t_diff*v)/(s*t_diff)
    w2 = A/(s*t_diff)
    
    ##use jax.scipy.stats here, some difference is there between matlab /python
    x = norm.cdf(w1-w2)
    x1 = norm.pdf(w1-w2)
    z = norm.cdf(w1) 
    z1 = norm.pdf(w1)
    
    #to avoid numerical error (as in matlab code) comment this if using jax numpy (handled in hybrid VAFC)
    # id = x> 0.999
    # x[id] = 0.999
    # id = x<=0.001
    # x[id] = 0.001
    #
    # id = z>0.999 #CVVB_forstman model 10(m=9), 28 values, Sep 2
    # z[id] = 0.999
    # id = z<0.001 #CVVB_forstman model 10(m=9), 4 values , Sep 2
    # z[id] = 0.001

    #x = (x + 10 ** (-36)) * 0.9999        #added for model 10 CVVB forstmann
    #z = (z+10**(-36))*0.9999              #added for model 10 CVVB forstmann

    f_c_t= (1/A)*( v*(z-x) + s*(x1-z1))
    
    #to avoid numerical zero devision (comment if using jax numpy)
    # ind_fc = f_c_t <= 10**(-50)
    # f_c_t[ind_fc]  = 10**(-50)

    #ind_fc = f_c_t <= 10**(-50)
    #jax.ops.index_update(f_c_t,jax.ops.index[ind_fc],10**(-30))
    #jax.ops.index_update(f_c_t,jax.ops.index[1:] , 10 ** (-50))
    #f_c_t[ind_fc]  = 10**(-50)
    #f_c_t=f_c_t+ 10**(-36)   # This solution is added on Sep 2 as outside hybrid_    166.2730 to 166.21574
                                  # VAFC check can't do much when all MC samples has inf/nan
                                  # checked npj.sum(npj.log(npj.array(f_c_t)) and it remains same for 4 decimals.

    #if (gradient == "true")
    x2 = phi_prime(w1 - w2)
    z2 = phi_prime(w1)

    grad_b = (1 / A)* (v* (z1 - x1) + s* (x2 - z2)) / (t_diff* s)
    grad_A = -(1. / A)* (f_c_t + (-v* x1 + s* x2)/ (s* t_diff))
    grad_v = (1. / A)* (z - x) + (1. / A)* (v* (z1 - x1) + s* (x2 - z2)) / (-s)
    grad_s = (1. / A)* ((s* x2 - v* x1)* ((w2 - w1)/ s) + (v* z1 - s* z2)* (-w1/ s) + x1 - z1)
    grad_tau = (1. / A)* ((-v* x1 + s* x2)* (b - A)/ (s* (t_diff ** 2)) + (v* z1 - s* z2)* b/ (
                    s* (t_diff** 2)))

    output_grad_b = grad_b/ f_c_t
    output_grad_A = grad_A/ f_c_t
    output_grad_v = grad_v/ f_c_t
    output_grad_s = grad_s/ f_c_t
    output_grad_tau = grad_tau/ f_c_t

    output_grad= npj.hstack((output_grad_b,output_grad_A,output_grad_v,output_grad_s,output_grad_tau))
    return (npj.sum(npj.log(npj.array(f_c_t))),output_grad)


#test 
# output=p_Sigma_alpha(Sigma_alpha,v,v_a,Psi,alpha,mu_alpha,a)
# output_log=output[0]
# output_grad=output[1]
