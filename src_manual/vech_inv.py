#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu 27 9:44:23 2021
This is to generate vech(A) function
This function is generated for jax here
Need jax library

"""
import numpy as npj
import jax.numpy as npj
import jax
#import time

def vech_inv(v,dim):
    A=npj.ones([dim,dim])
    lower_A=npj.tril(A) 
    ind_non_zero= npj.transpose(npj.array(npj.nonzero(lower_A)))
    ind_non_zero_sorted = npj.lexsort((ind_non_zero[:,0],ind_non_zero[:,1])) #  sort the indexes to fill by column wise first
    
    new_indexs=ind_non_zero[ind_non_zero_sorted,:]
    
    #B= np.zeros([dim,dim])
    B1=npj.zeros([dim,dim])
    
    for i in range(new_indexs.shape[0]):
        #B[new_indexs[i,0],new_indexs[i,1]]=v[i]
        B1=jax.ops.index_update(B1,(new_indexs[i,0],new_indexs[i,1]) , v[i])
    
    return B1



#x1=vech(x)


