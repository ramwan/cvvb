#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26 11:44:02 2021
This is to genrate the log of multi variate gamma
log_multigamma(a,p) is generated via two ways
 
Both ways are compatible for jax

"""
# import numpy as npj
# from scipy.special import gamma
# from scipy.special import multigammaln
# from scipy.special import gammaln

#import jax.numpy as npj
import numpy as npj
#from jax.scipy.special import multigammaln
#from jax.scipy.special import gammaln
from scipy.special import gammaln
#import time

def log_multigamma(a,p):
 
    j= npj.arange(1,p+1).T
    y=p*(p-1)/4*npj.log(npj.pi) +npj.sum(gammaln(a + (1-j)/2))
    return y


# def log_multigamma(a,p):
#     y=multigammaln(a,p)
#     return multigammaln(a,p)


#Check rinning time
#start= time.time()
# start= time.time()
# print(multigammaln(13.5,7)) 
# end_time=time.time()
# print("time",end_time-start) 

# start= time.time()
#print(log_multigamma(13.5,7))
# end_time=time.time()
# print("time",end_time-start) 
#ans 134.84064808018212
#it is taking similar time


