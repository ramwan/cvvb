#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26 17:36:12 2021
This function calculates the fc(t) component.
The gradient is not calculated here as in MATLAB 

input:LBA_pdf(c,t,b,A,v,s,tau,gradient)
output: (log_LBA, grad_log_LBA)

"""
import numpy
import numpy as npj
import numpy as np

#if using jax numpy
# import numpy
# import jax.numpy as npj
# import jax.numpy as np

#from jax.scipy.stats import norm
from CDF_c_v2 import cdf_c_v2
from pdf_c_v2 import pdf_c_v2

# # log of LBA= sum(log elementwise)
# def obtain_log_pdf_LBA (c,t,bAvs_tau):
#
#     # start= time.time()
#     #c,t,b,A,v,s,tau
#     c=npj.array(c)
#     t=npj.array(t)
#
#     b=bAvs_tau[:,:2]
#     A=bAvs_tau[:,2:4]
#     v=bAvs_tau[:,4:6]
#     s=bAvs_tau[:,6:8]
#     tau=bAvs_tau[:,8:10]
#
#     ind_acc2= c==2  #not true
#     ind_acc1= c==1 # truesß
#
#     b_c=ind_acc2*b[:,1].reshape(-1,1)+ ind_acc1*b[:,0].reshape(-1,1) #3.59ms
#     A_c=ind_acc2*A[:,1].reshape(-1,1)+ ind_acc1*A[:,0].reshape(-1,1) #3.44ms
#     v_c=ind_acc2*v[:,1].reshape(-1,1)+ ind_acc1*v[:,0].reshape(-1,1)
#     s_c=ind_acc2*s[:,1].reshape(-1,1)+ ind_acc1*s[:,0].reshape(-1,1)
#     tau_c=ind_acc2*tau[:,1].reshape(-1,1)+ ind_acc1*tau[:,0].reshape(-1,1)
#
#
#     b_k=ind_acc2*b[:,0].reshape(-1,1)+ ind_acc1*b[:,1].reshape(-1,1)
#     A_k=ind_acc2*A[:,0].reshape(-1,1)+ ind_acc1*A[:,1].reshape(-1,1)
#     v_k=ind_acc2*v[:,0].reshape(-1,1)+ ind_acc1*v[:,1].reshape(-1,1)
#     s_k=ind_acc2*s[:,0].reshape(-1,1)+ ind_acc1*s[:,1].reshape(-1,1)
#     tau_k=ind_acc2*tau[:,0].reshape(-1,1)+ ind_acc1*tau[:,1].reshape(-1,1)
#
#
#     #method1 sum f_c and F_k here as in matlab
#     # f_c=jit(pdf_c)(t, b_c, A_c, v_c, s_c, tau_c)
#
#     # F_k=jit(cdf_c)(t, b_k, A_k, v_k, s_k, tau_k)
#
#     # #log of LBA
#     # log_LBA=npj.sum(npj.log(f_c)+ npj.log(1-F_k))
#
#
#
#     #method2 used summed log f_c and log (1-F_k) here
#
#     #f_c=pdf_c(t, b_c, A_c, v_c, s_c, tau_c)
#
#     #log(1-F_k) sum
#     #F_k=cdf_c(t, b_k, A_k, v_k, s_k, tau_k)
#
#     #jit application to speedup
#     f_c = pdf_c(t, b_c, A_c, v_c, s_c, tau_c)
#
#     # log(1-F_k) sum
#     F_k = cdf_c(t, b_k, A_k, v_k, s_k, tau_k)
#
#     #log of LBA
#     log_LBA=f_c[0]+ F_k[0]
#
#     grad_b_acc1 = f_c[1][:,0].reshape(-1,1)* ind_acc1 + F_k[1][:,0].reshape(-1,1)* ind_acc2
#     grad_A_acc1 = f_c[1][:,1].reshape(-1,1)* ind_acc1 + F_k[1][:,1].reshape(-1,1)* ind_acc2
#     grad_v_acc1 = f_c[1][:,2].reshape(-1,1)* ind_acc1 + F_k[1][:,2].reshape(-1,1)* ind_acc2
#     grad_s_acc1 = f_c[1][:,3].reshape(-1,1)* ind_acc1 + F_k[1][:,3].reshape(-1,1)* ind_acc2
#     grad_tau_acc1 = f_c[1][:,4].reshape(-1,1)* ind_acc1 + F_k[1][:,4].reshape(-1,1)* ind_acc2
#
#     grad_b_acc2 = f_c[1][:,0].reshape(-1,1)* ind_acc2 + F_k[1][:,0].reshape(-1,1)* ind_acc1
#     grad_A_acc2 = f_c[1][:,1].reshape(-1,1)* ind_acc2 + F_k[1][:,1].reshape(-1,1)* ind_acc1
#     grad_v_acc2 = f_c[1][:,2].reshape(-1,1)* ind_acc2 + F_k[1][:,2].reshape(-1,1)* ind_acc1
#     grad_s_acc2 = f_c[1][:,3].reshape(-1,1)* ind_acc2 + F_k[1][:,3].reshape(-1,1)* ind_acc1
#     grad_tau_acc2 = f_c[1][:,4].reshape(-1,1)* ind_acc2 + F_k[1][:,4].reshape(-1,1)* ind_acc1
#
#     output_grad=np.hstack((grad_b_acc1,grad_b_acc2,grad_A_acc1,grad_A_acc2,grad_v_acc1,grad_v_acc2,grad_s_acc1,grad_s_acc2,grad_tau_acc1,grad_tau_acc2))
#
#
#     return (npj.array(log_LBA),output_grad)


# log of LBA= sum(log elementwise)
def obtain_log_pdf_LBA_man(c, t, b,A,v,s,tau, gradient):
    # start= time.time()
    # c,t,b,A,v,s,tau
    c = npj.array(c)
    t = npj.array(t)

    # b = bAvs_tau[:, :2]
    # A = bAvs_tau[:, 2:4]
    # v = bAvs_tau[:, 4:6]
    # s = bAvs_tau[:, 6:8]
    # tau = bAvs_tau[:, 8:10]

    ind_acc2 = c == 2  # not true
    ind_acc1 = c == 1  # truesß

    b_c = ind_acc2 * b[:, 1].reshape(-1, 1) + ind_acc1 * b[:, 0].reshape(-1, 1)  # 3.59ms
    A_c = ind_acc2 * A[:, 1].reshape(-1, 1) + ind_acc1 * A[:, 0].reshape(-1, 1)  # 3.44ms
    v_c = ind_acc2 * v[:, 1].reshape(-1, 1) + ind_acc1 * v[:, 0].reshape(-1, 1)
    s_c = ind_acc2 * s[:, 1].reshape(-1, 1) + ind_acc1 * s[:, 0].reshape(-1, 1)
    tau_c = ind_acc2 * tau[:, 1].reshape(-1, 1) + ind_acc1 * tau[:, 0].reshape(-1, 1)

    b_k = ind_acc2 * b[:, 0].reshape(-1, 1) + ind_acc1 * b[:, 1].reshape(-1, 1)
    A_k = ind_acc2 * A[:, 0].reshape(-1, 1) + ind_acc1 * A[:, 1].reshape(-1, 1)
    v_k = ind_acc2 * v[:, 0].reshape(-1, 1) + ind_acc1 * v[:, 1].reshape(-1, 1)
    s_k = ind_acc2 * s[:, 0].reshape(-1, 1) + ind_acc1 * s[:, 1].reshape(-1, 1)
    tau_k = ind_acc2 * tau[:, 0].reshape(-1, 1) + ind_acc1 * tau[:, 1].reshape(-1, 1)


    f_c = pdf_c_v2(t, b_c, A_c, v_c, s_c, tau_c, gradient)

    # log(1-F_k) sum
    #F_kp = cdf_c_v2(t, b_k, A_k, v_k, s_k, tau_k,gradient)
    F_k = cdf_c_v2(t, b_k, A_k, v_k, s_k, tau_k, gradient)

    # log of LBA
    log_LBA = f_c[0] + F_k[0]

    if gradient == True:

        grad_b_acc1 = f_c[1][:, 0].reshape(-1, 1) * ind_acc1 + F_k[1][:, 0].reshape(-1, 1) * ind_acc2
        grad_A_acc1 = f_c[1][:, 1].reshape(-1, 1) * ind_acc1 + F_k[1][:, 1].reshape(-1, 1) * ind_acc2
        grad_v_acc1 = f_c[1][:, 2].reshape(-1, 1) * ind_acc1 + F_k[1][:, 2].reshape(-1, 1) * ind_acc2
        grad_s_acc1 = f_c[1][:, 3].reshape(-1, 1) * ind_acc1 + F_k[1][:, 3].reshape(-1, 1) * ind_acc2
        grad_tau_acc1 = f_c[1][:, 4].reshape(-1, 1) * ind_acc1 + F_k[1][:, 4].reshape(-1, 1) * ind_acc2

        grad_b_acc2 = f_c[1][:, 0].reshape(-1, 1) * ind_acc2 + F_k[1][:, 0].reshape(-1, 1) * ind_acc1
        grad_A_acc2 = f_c[1][:, 1].reshape(-1, 1) * ind_acc2 + F_k[1][:, 1].reshape(-1, 1) * ind_acc1
        grad_v_acc2 = f_c[1][:, 2].reshape(-1, 1) * ind_acc2 + F_k[1][:, 2].reshape(-1, 1) * ind_acc1
        grad_s_acc2 = f_c[1][:, 3].reshape(-1, 1) * ind_acc2 + F_k[1][:, 3].reshape(-1, 1) * ind_acc1
        grad_tau_acc2 = f_c[1][:, 4].reshape(-1, 1) * ind_acc2 + F_k[1][:, 4].reshape(-1, 1) * ind_acc1

        output_grad = np.hstack((grad_b_acc1, grad_b_acc2, grad_A_acc1, grad_A_acc2, grad_v_acc1, grad_v_acc2, grad_s_acc1,
                                 grad_s_acc2, grad_tau_acc1, grad_tau_acc2))

        print(np.shape(output_grad))
    
        return (npj.array(log_LBA), output_grad)

    else:
        log_element= f_c[2] + F_k[2]
        return (npj.array(log_LBA), npj.array([0]),log_element)  # additionally sent log element wise


def join_bAvs_tau (b,A,v,s,tau):
    
    bAvs_tau=np.concatenate((b.T, A.T,v.T,s.T,tau.T)).T
    
    return bAvs_tau


def LBA_pdf_v2(c,t,b,A,v,s,tau,gradient):

    output_val_grad = obtain_log_pdf_LBA_man(c, t, b, A, v, s, tau, gradient)

    temp = list(output_val_grad)
    del output_val_grad


    if gradient ==True:
        output_val_grad = (numpy.array(temp[0]), numpy.array(temp[1]))
        return output_val_grad
    else:
       # print('likelihood log value only')
        output_val_grad = (numpy.array(temp[0]), numpy.array(temp[1]),numpy.array(temp[2]))
        return output_val_grad
    

#test 
# output=p_Sigma_alpha(Sigma_alpha,v,v_a,Psi,alpha,mu_alpha,a)
# output_log=output[0]
# output_grad=output[1]
