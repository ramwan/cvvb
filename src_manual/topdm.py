# -*- coding: utf-8 -*-
"""
Created on Fri May 28 02:05:20 2021

# FUNCTION THAT TRANSFORMS A NON POSITIVE DEFINITE SYMMETRIC
# MATRIX -E.G. A COVARIANCE MATRIX- TO A 
# POSITIVE DEFINITE SYMMETRIC MATRIX. 

"""
import numpy as np
from numpy import linalg

def topdm(sig): 


    sigma = sig 


    try:
        np.linalg.cholesky(sigma)
        return sigma
    except np.linalg.linalg.LinAlgError as err:
        
        eps = 10**(-6)      #SET THE VALUE TO PLACE INSTEAD OF ZERO OR NEGATIVE EIGENVALUE
        zero = 10**(-10)    #SET THE VALUE TO LOOK FOR 
       
    
       # [v, d] = np.linalg.eig(sigma) #matlab way
        [d, v] = np.linalg.eig(sigma) #CALCULATE EIGENVECTOR AND EIGENVALUES 
        #d=np.diag(d);          #GET A VECTOR OF EIGENVALUES 
        d.flags.writeable = True
        d[d<zero]=eps #FIND ALL EIGENVALUES<=ZERO AND CHANGE THEM FOR EPS 
         
        d=np.diag(d)      #CONVERT VECTOR d INTO A MATRIX 
    
        sigma = v @ d @v.T #RECOMPOSE SIGMA MATRIX USING EIGENDECOMPOSITION 
                         #WHY? SIGMA IS SIMETRIC AND V IS ORTHONORMAL 
        return sigma                   
    
#x1=topdm(inp)