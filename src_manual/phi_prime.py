import numpy as np
#from scipy.stats import norm
from jax.scipy.stats import norm
def phi_prime(x):
    return -norm.pdf(x) * x

