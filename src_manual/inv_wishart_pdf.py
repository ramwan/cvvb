#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26 17:44:11 2021
This is to generate inverse wishart pdf (with Jax compatibility)

np.stats.invwishart([df, scale, seed]) can we use this ?

"""
import numpy as npj
from numpy.linalg import slogdet #not available in jax
#import jax.numpy as npj
from multigamma import multigamma
from scipy.stats import invwishart

#import time

def inv_wishart_pdf(X,Psi,v):

    p = Psi.shape[0]
    output = 0.5*v*slogdet(Psi)[1] - 0.5*v*p*npj.log(2) - npj.log(multigamma(v/2,p)) -0.5*(v+p+1)*slogdet(X)[1] - 0.5*npj.trace(Psi/X)
    
    return output


#print(inv_wishart_pdf(x1,x1/2,1))

# def inv_wishart_pdf2(X,Psi,v):

    
#     output = inv_wishart_pdf(X,Psi,v)
#     return output



