#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 23 2021
This is main CVVB code using the Hybrid VB for Hierarchical LBA model.
@author: Gajan
"""

#load standard libraries 
import numpy as np
import scipy.io as sio
from scipy.stats import invwishart


#load other libraries 
from lib_lba import ValGrad,Model,PriorHyperParams,LearningRateADADELTA,LearningRateADAM,Output,InitialMCMC,VbSettings,Lambda1
from vech import vech
from Hybrid_VAFC import Hybrid_VAFC # without parralel processing
from likelihood_hybrid import likelihood_Hybrid
from sklearn.model_selection import KFold
import copy
import time, pickle
import os

#added on August 24
from ModelSpecification import model_specification
from vb_adaptive_initialization import vb_adaptive_initialization


#load data set

if __name__=='__main__':
    #step0 data preparation

    #exp_name='My_experiment'
    exp_name = 'Forstmann'
    dataset_name='Forstmann.mat'

    # set the path accordingly to your pc path/HPC path
    hpc_path='./' # for hpc
    #dataset_path=os.path.join(hpc_path,dataset_name) #hpc
    dataset_path = os.path.join(dataset_name) #pc

    K =5 # no of folds

    #load one model initial
    #intital_path=os.path.join('initial_MCMC.mat')
    # initial_MCMC= sio.loadmat(intital_path)

    # load 27 models initial
    #set the path accordingly to your pc path/HPC path
    #intital_path_models=os.path.join('intial_values_forstmann.mat') #pc
    intital_path_models = os.path.join(hpc_path, 'intial_values_forstmann.mat') #hpc

    # dmat= sio.loadmat(intital_path_models)
    # initial_MCMC_models=dmat['initial_MCMC_Forstmann']
    # temp1=initial_MCMC_models[1][0]

    mat = sio.loadmat(dataset_path)  # load mat-file
    mdata = mat['data']  # variable in mat file
    mdtype = mdata.dtype  # dtypes of structures are "unsized objects"
    data = {n: mdata[n][0, 0] for n in mdtype.names}

    #VB Tuning parameters
    #max_iter = 100  # number of iterations - 5000 at least
    max_iter = 10000
    max_norm = 1000  # upper bound the the norm of the gradients

    #r = 1  # B has size dxr - at least 10
    r = 15
    I = 10  # number of Monte Carlo samples to estimate the gradients
    #window = 5 # 100 
    window = 200
    patience_parameter = 20  # 50
    a_t = 0.9  # temperature for annealing

    learning_rate = LearningRateADADELTA(0.95, 10 ** (-7))

    silent = True# False #display estimates for at each iteration
    generate_samples = False

    #
    lambda1 = Lambda1(np.array([0]) , np.array([0]) ,np.array([0]))

    vb_settings=VbSettings(r, max_iter, max_norm, I, window, patience_parameter, silent, generate_samples, lambda1,learning_rate)


    if exp_name == 'My_experiment':
        model_index = np.array([3, 1, 2, 0, 1])
        model_name = np.array([3, 1, 1, 0, 1])
        D_latent = np.sum(model_index)
        prior_par_mu = np.zeros([D_latent, 1])
        prior_par_cov = np.eye(D_latent)
        prior_par_v_a = 2
        prior_par_A_d = np.ones([D_latent, 1])

        J=19
        M=1
        p = D_latent*(J+2) # dim(theta) = p

        prior_par = PriorHyperParams(prior_par_mu, prior_par_cov, prior_par_v_a, prior_par_A_d)

        model1 = Model(model_name, model_index, J, p, prior_par)
        model= list()
        model.append(model1)

    else:
        M,model = model_specification('Forstmann')


    # step2 randomly partition the data in Kfolds
    J = len(data['RE'])
    kf = KFold(n_splits=K, random_state=0, shuffle=True)
    data_fields = list(data.keys())

    train_ind = list()
    test_ind = list()
    for j in range(J):
        RT = data['RT'][j][0]

        train_j = list() #save subject train index for K folds
        test_j = list()  #save subject train index for K folds

        #loop for K folds
        for train_index, test_index in kf.split(RT):
            train_j.append(train_index)
            test_j.append(test_index)

        train_ind.append(train_j)
        test_ind.append(test_j)
        del train_j
        del test_j



    #Step 3: Kfold CVVB algorithm
    elpd_cvvb = np.zeros([K,M]) #store all ELPD estimates
    vb_results = list() #store all VB results
    total_time=0 #count time
    #M=1
    #for m in range(M):  #for m in range(1,M):
    for m in [0]:

        time1 = time.time()
        D_alpha = int(np.sum(model[m].index)) # Also same as D_latent,  number of random effects per participant
        p = D_alpha * (J + 2)  # dim(theta) = p

        #not needed in python
        # model{m}.num_subjects = J;
        # model{m}.matching_function_1 = matching_function_1;
        # model{m}.matching_function_2 = matching_function_2;
        model_name=(' c ~'+ str(model[m].name[0]) + '| A ~ '+ str(model[m].name[1]) + '| v ~ ' + str( model[m].name[2]) + '| s ~ '+ str(model[m].name[3]) +'| t0 ~ '+ str(model[m].name[4])  )
        print('Model '+ str(m+1)+ str(model_name))

        ## VB initialization (CMC MC part)
        r= vb_settings.r

        if np.sum(model[m].index) < 10:
            epsilon=1 #scale parameter of covariance matrix
            N_iter=100 # number of iterations in PMwG to initialize VB
        elif  np.sum(model[m].index) < 20:
            epsilon=0.6 #scale parameter of covariance matrix
            N_iter=150
        else:
            epsilon=0.3 #scale parameter of covariance matrix
            N_iter=150


        R = 10 # number of particles in PMwG
        initial_MCMC_alpha = np.random.randn(D_alpha,J)
        initial_MCMC_mu = np.random.randn(D_alpha,1) # the initial values for parameter \mu
        initial_MCMC_sig2 = invwishart.rvs(D_alpha + 10,np.eye(D_alpha)) # the initial values for \Sigma
        initial_MCMC_a_d = 1/np.random.gamma(1/2,1,(D_alpha,1))

        initial_MCMC= InitialMCMC(initial_MCMC_alpha,initial_MCMC_mu,initial_MCMC_sig2,initial_MCMC_a_d)

        #CVVB for the current model
        k=0
        vb_results_model_m= list() #save VB results of model m
        while k < K:
            train_data = copy.deepcopy(data)   #dict()
            test_data = copy.deepcopy(data)

            for j in range (J):
                for fields in range(len(data_fields)):
                    tmp_field = data_fields[fields]
                    tmp_field_data = train_data[tmp_field][j][0]
                    train_data[tmp_field][j][0] = tmp_field_data[train_ind[j][k]]

                    test_data[tmp_field][j][0] = tmp_field_data[test_ind[j][k]]

                #print(1)


            if k == 0:
                #if for checking one model as hybrid_VB /my experiment
                #if using 100% data
                #(MCMC_initial,other)=vb_adaptive_initialization(model[m],data,initial_MCMC,N_iter,R,epsilon)

                #if using Vb adapative_intial (not converging as matlab, please check )
                #(MCMC_initial, other) = vb_adaptive_initialization(model[m], train_data, initial_MCMC, N_iter, R, epsilon)
                #initial = np.vstack([np.reshape(MCMC_initial[3], (D_alpha * J, 1)),MCMC_initial[0].reshape(-1,1),np.log(MCMC_initial[2]).reshape(-1,1)])
                #lambda_mu=initial.squeeze()


                # if for checking 27 models of Forstsmann using matlab intial values as CVVB experiment
                dmat= sio.loadmat(intital_path_models)
                lambda_mu = dmat['initial_MCMC_Forstmann'][m][0].flatten()

                lambda_B = np.zeros([p, r]) / r
                lambda_B = np.tril(lambda_B)
                lambda_d = 0.01 * np.ones([p, 1])

                #lambda_mu=np.zeros([p,1]) #just for check
                lambda1 = Lambda1(lambda_mu, lambda_B, lambda_d)

            vb_settings.initial = lambda1
            output = Hybrid_VAFC(model[m], train_data, vb_settings)
            lambda1 = output.lambda1
            vb_results_model_m.append(output)




            #Compute the Kfold CVVB ELPD Estimation
            # if output.converge == True | output.converge == 'reach_max_iter':
            if (output.converge == True) | (output.converge == 'reach_max_iter'):

                mu = lambda1.mu
                B = lambda1.B
                d = lambda1.d

                N = 10  # number of samples to estimate ELPD
                log_pdfs = np.zeros([N,1])
                model_current = model[m]
                for n in range(0, N):
                    epsilon = np.random.randn(p, 1)
                    z = np.random.randn(r, 1)
                    theta_1 = (mu.reshape(-1, 1) + B @ z + d.reshape(-1,
                                                                     1) * epsilon).squeeze()  # theta_1 = (alpha_1,...,alpha_J,mu_alpha,log a_1,...,log a_D)
                    alpha = np.reshape(theta_1[:D_alpha * J], [D_alpha, J], order='F')
                    like = likelihood_Hybrid(model_current, alpha, test_data, False)
                    log_pdfs[n] = like.val   #get loglikelihood
                    #print(n)

                max_log = np.max(log_pdfs)
                pdfs_ratio=np.exp(log_pdfs-max_log)
                elpd_cvvb[k,m] = max_log + np.log(np.mean(pdfs_ratio))

#                print(m)

                print(['Fold' + str(k) +' || ELPD = ' + str(np.round(elpd_cvvb[k,m])) + ' || Initial LB = ' + str (output.LB[0]) +
                       ' || max LB = ' + str (output.max_LB) + ' || N_iter = ' + str (len(output.LB))])
                k=k+1
            else:
                print('This model is not well approximated, skip')
                k= K+1 # to skip next folds for same model

            #save model m all cross validation vb results
        time2 = time.time()

        print(['K fold CVVB estimate of ELPD = ' + str(np.mean(elpd_cvvb[:,m]))+ ', K fold run time ' + str((time2-time1)/60) + ' minutes'])

        total_time = total_time + (time2 - time1)
        vb_results.append(vb_results_model_m)
        del vb_results_model_m

            #k = k+1 looping is done in the above if/else statement

   # start = time.time()
    f = open('output_cvvb_Sep13_w100_p20_i200.pickle', 'wb')
    pickle.dump([model,vb_results,elpd_cvvb], f)
    #pickle.dump([model,prior_par,initial_MCMC['initial'],output], f)
    f.close()

    print(['total run time ' + str(total_time/60) +' minutes'])





