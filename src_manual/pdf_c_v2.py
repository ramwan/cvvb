#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26 17:36:12 2021
This function calculates the fc(t) component.
The gradient is not calculated here as in MATLAB 
pdf_c(t,b,A,v,s,tau,gradient) is changed to pdf_c(t,b,A,v,s,tau) in python

"""
# import numpy as jnp
# from scipy.stats import norm


#jax enable
import jax.numpy as jnp
from jax.scipy.stats import norm
from phi_prime_v2 import phi_prime_v2
from jax import value_and_grad, vmap, jit
import time

def pdf_c_v2(t,b,A,v,s,tau,gradient):
    t_diff = t-tau

    #shortcut computation (see paper page 5 for notation x,y,z,u)
    w1 = (b - t_diff*v)/(s*t_diff)
    w2 = A/(s*t_diff)
    
    ##use jax.scipy.stats here, some difference is there between matlab /python
    x = norm.cdf(w1-w2)
    x1 = norm.pdf(w1-w2)
    z = norm.cdf(w1) 
    z1 = norm.pdf(w1)
    
    #to avoid numerical error (as in matlab code) comment this if using jax numpy (handled in hybrid VAFC)
    """
    id = x> 0.999
    x[id] = 0.999
    id = x<=0.001
    x[id] = 0.001

    id = z>0.999
    z[id] = 0.999
    id = z<0.001
    z[id] = 0.001
    """

    f_c_t= (1/A)*( v*(z-x) + s*(x1-z1))
    
    #to avoid numerical zero devision (comment if using jax numpy)
    """
    ind_fc = f_c_t <= 10**(-50)
    f_c_t[ind_fc]  = 10**(-50)
    """

    if gradient == True:
        x2 = phi_prime_v2(w1 - w2)
        z2 = phi_prime_v2(w1)

        grad_b = (1 / A)* (v* (z1 - x1) + s* (x2 - z2)) / (t_diff* s)
        grad_A = -(1. / A)* (f_c_t + (-v* x1 + s* x2)/ (s* t_diff))
        grad_v = (1. / A)* (z - x) + (1. / A)* (v* (z1 - x1) + s* (x2 - z2)) / (-s)
        grad_s = (1. / A)* ((s* x2 - v* x1)* ((w2 - w1)/ s) + (v* z1 - s* z2)* (-w1/ s) + x1 - z1)
        grad_tau = (1. / A)* ((-v* x1 + s* x2)* (b - A)/ (s* (t_diff ** 2)) + (v* z1 - s* z2)* b/ (
                        s* (t_diff** 2)))

        output_grad_b = grad_b/ f_c_t
        output_grad_A = grad_A/ f_c_t
        output_grad_v = grad_v/ f_c_t
        output_grad_s = grad_s/ f_c_t
        output_grad_tau = grad_tau/ f_c_t

        output_grad= jnp.hstack((output_grad_b,output_grad_A,output_grad_v,output_grad_s,output_grad_tau))
        return (jnp.sum(jnp.log(jnp.array(f_c_t))),output_grad)

    else:
        return (jnp.sum(jnp.log(jnp.array(f_c_t))), jnp.array([0]),jnp.log(jnp.array(f_c_t))) # additionally sent log element wise



#test 
# output=p_Sigma_alpha(Sigma_alpha,v,v_a,Psi,alpha,mu_alpha,a)
# output_log=output[0]
# output_grad=output[1]

@jit
def pdf_c_v2_ad_grad(t,b,A,v,s,tau):
    w1 = lambda b1, t1, v1, s1, tau1: jnp.divide((b1 - jnp.multiply(t1 - tau1, v1)), jnp.multiply(s1, t1 - tau1))
    w2 = lambda A1, s1, t1, tau1: jnp.divide(A1, jnp.multiply(s1, t1 - tau1))

    x = lambda b1, t1, v1, s1, A1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    x1 = lambda b1, t1, v1, s1, A1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    z = lambda b1, t1, v1, s1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1))
    z1 = lambda b1, t1, v1, s1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1))

    #f_c_t= (1/A)*( v*(z-x) + s*(x1-z1))

    f_c_t = lambda b1, A1, v1, s1, t1, tau1: \
        jnp.sum(jnp.log(jnp.divide(
            jnp.multiply(v1, z(b1, t1, v1, s1, tau1) - x(b1, t1, v1, s1, A1, tau1))\
            + jnp.multiply(s1, x1(b1, t1, v1, s1, A1, tau1) - z1(b1, t1, v1, s1, tau1))\
            , A1)
        ))
    output_grad = value_and_grad(f_c_t, (0, 1, 2, 3, 5))
    (val, grads) = vmap(output_grad)(b, A, v, s, t, tau)
    val = jnp.sum(val)
    grads = jnp.hstack(grads)

    return (val, grads)

def pdf_c_v2_no_grad(t,b,A,v,s,tau):
    w1 = lambda b1, t1, v1, s1, tau1: jnp.divide((b1 - jnp.multiply(t1 - tau1, v1)), jnp.multiply(s1, t1 - tau1))
    w2 = lambda A1, s1, t1, tau1: jnp.divide(A1, jnp.multiply(s1, t1 - tau1))

    x = lambda b1, t1, v1, s1, A1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    x1 = lambda b1, t1, v1, s1, A1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    z = lambda b1, t1, v1, s1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1))
    z1 = lambda b1, t1, v1, s1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1))

    #f_c_t= (1/A)*( v*(z-x) + s*(x1-z1))

    f_c_t = lambda b1, A1, v1, s1, t1, tau1: \
        jnp.sum(jnp.log(jnp.divide(
            jnp.multiply(v1, z(b1, t1, v1, s1, tau1) - x(b1, t1, v1, s1, A1, tau1))\
            + jnp.multiply(s1, x1(b1, t1, v1, s1, A1, tau1) - z1(b1, t1, v1, s1, tau1))\
            , A1)
        ))
    val = f_c_t(b, A, v, s, t, tau)

    return (jnp.sum(val), jnp.array([0]), val)

def diff_manual_ad(t, b, A, v, s, tau, gradient):
    if gradient:
        startm = time.perf_counter()
        vm, gm = pdf_c_v2(t, b, A, v, s, tau, gradient)
        endm = time.perf_counter()
        va, ga = pdf_c_v2_ad_grad(t, b, A, v, s, tau)
        enda = time.perf_counter()

        print("value diff: ", jnp.abs(vm - va))
        print("gradient diff: ", jnp.abs(jnp.vdot(gm - ga, gm - ga)))
        print("manual time (ms): ", (endm-startm)*1000 )
        print("auto jit time (ms): ", (enda-endm)*1000 )
    else:
        print("nein")
