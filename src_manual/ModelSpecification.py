from lib_lba import ValGrad,Model,PriorHyperParams,LearningRateADADELTA,LearningRateADAM,Output,VbSettings,Lambda1
import numpy as np



def model_specification (dataset_name):
    if (dataset_name== 'Forstmann'):
        M=27 # total models
       # M=3 #example_test
        model = list()


        model_ind= np.array([[1, 1, 1],
                             [1, 1, 2],
                             [1, 1, 3],
                             [1, 2, 3],
                             [1, 2, 2],
                             [1, 2, 1],
                             [1, 3, 1],
                             [1, 3, 2],
                             [1, 3, 3],
                             [2, 3, 3],
                             [2, 3, 2],
                             [2, 3, 1],
                             [2, 2, 1],
                             [2, 2, 2],
                             [2, 2, 3],
                             [2, 1, 3],
                             [2, 1, 2],
                             [2, 1, 1],
                             [3, 1, 1],
                             [3, 1, 2],
                             [3, 1, 3],
                             [3, 2, 3],
                             [3, 2, 2],
                             [3, 2, 1],
                             [3, 3, 1],
                             [3, 3, 2],
                             [3, 3, 3]])  # each value corresponcds to z_c, z_v, z_tau

        model_constraints=np.array([1,2,3])
        for i in range(M):
            ind=model_ind[i,:]
            model_name = np.array([3, 1, 1, 0, 1])
            model_name = np.array([model_constraints[ind[0]-1], 1 ,model_constraints[ind[1]-1] , 0 , model_constraints[ind[2]-1]])
            model_index = np.zeros([5,1],dtype=int)
            model_index[0] = ind[0]
            model_index[1] = 1
            model_index[2] = ind[1]
            model_index[4] = ind[2]

            model_index[2] = 2*model_index[2]


            D_latent = np.int(np.sum(model_index))
            prior_par_mu = np.zeros([D_latent, 1])
            prior_par_cov = np.eye(D_latent)
            prior_par_v_a = 2
            prior_par_A_d = np.ones([D_latent, 1])

            prior_par = PriorHyperParams(prior_par_mu, prior_par_cov, prior_par_v_a, prior_par_A_d)

            # model=Model(model_name,model_index,J,p)
            J = 19
            p = D_latent * (J + 2)
            model.append(Model(model_name, model_index.squeeze(), J, p, prior_par))

        return M,model
        #
        # model_ind= [[1, 1, 1],; 1 1 2; 1 1 3; 1 2 3; 1 2 2; 1 2 1; 1 3 1; 1 3 2;
        # 1 3 3;  2 3 3; 2 3 2; 2 3 1; 2 2 1; 2 2 2; 2 2 3; 2 1 3; 2 1 2; 2 1 1;
        # 3 1 1; 3 1 2; 3 1 3; 3 2 3; 3 2 2; 3 2 1; 3 3 1; 3 3 2; 3 3 3]


    else:
        print('invalid data set name')