#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri 28 9:21:12 2021
This function calculates the F_k(t) component.
The gradient is not calculated here as in MATLAB 
cdf_c(t,b,A,v,s,tau,gradient) is changed to cdf_c(t,b,A,v,s,tau) in python

"""
# import numpy as npj
# from scipy.stats import norm

import jax.numpy as npj
from jax.scipy.stats import norm
from phi_prime import phi_prime


def cdf_c(t,b,A,v,s,tau):     
    
    t_diff = t-tau

    #shortcut computation (see paper page 5 for notation x,y,z,u)
    w1 = (b - t_diff*v)/(s*t_diff)
    w2 = A/(s*t_diff)
    
    ##use jax.scipy.stats here, some difference is there between matlab /python
    x = norm.cdf(w1-w2)
    x1 = norm.pdf(w1-w2)
    z = norm.cdf(w1) 
    z1 = norm.pdf(w1)
    
    u1 = (b-A-t_diff*v)/A     
    u2 = (b-t_diff*v)/A

    
    #to avoid numerical error (as in matlab code) comment this if using jax numpy (handled in hybrid VAFC)
    # id = x > 0.999
    # x[id] = 0.999
    # id = x <= 0.001
    # x[id] = 0.001
    #
    # id = z > 0.999
    # z[id] = 0.999
    # id = z < 0.001
    # z[id] = 0.001

    #x = (x + 10 ** (-36)) * 0.9999        #added for model 10 CVVB forstmann
    #z = (z+10**(-36))*0.9999              #added for model 10 CVVB forstmann

    F_k_t_sub= -u1*x + u2*z - x1/w2 + z1/w2

    #to avoid numerical zero devision comment this if using jax numpy (handled in hybrid VAFC)
    # ind_fc = F_k_t_sub <= 10**(-50)
    # F_k_t_sub[ind_fc] = 10**(-50)
    #F_k_t_sub=F_k_t_sub + 10 ** (-36)   # This solution is added on Sep 2 as outside hybrid_
                                  # VAFC check can't do much when all MC samples has inf/nan
                                  # checked npj.sum(npj.log(npj.array(f_c_t)) and it remains same for 4 decimals.
    F_k_t= 1-F_k_t_sub

    x2 = phi_prime(w1 - w2)
    z2 = phi_prime(w1)
    w3 = ((b - A) / s) / (t_diff ** 2)

    grad_b = (1. / A)* x - z/ A + (u1* x1 - u2* z1 + (1/ w2)* (x2 - z2))/ (s* t_diff)
    grad_A = (-x + x1* A* u1/ (s* t_diff))/ A - (A * u1* x)/ A**2 + u2* z/ A + (
                x2* w2 - x1 + z1)/ (s* t_diff* w2**2)
    grad_v = -t_diff* x/ A + (u1* x1 - z1* u2)/ s + t_diff* z/ A + (x2 - z2)/ (s* w2)

    w12_s = (t_diff* v - b + A)/ (t_diff* s**2) # derivative

    w1_s = (t_diff* v - b)/ (t_diff* s**2) # derivative
    w2_s = -A/ (t_diff* s**2);
    grad_s = u1* x1* w12_s - u2* z1* w1_s + ((x2* w12_s - z2* w1_s)* w2 - (x1 - z1)* w2_s)/ w2**2
    grad_tau = (v/A)*x + u1*x1*w3 - (v/A)*z - u2*z1*(b/(s*t_diff**2)) +(x2*w2*w3 - x1*A/(s*t_diff**2) )/w2**2 - ( z2*w2*b/(s*t_diff**2) - z1*(A/(s*t_diff**2)) )/w2**2

    output_grad_b = -grad_b/F_k_t_sub # NOTE: term = 1- F_k(t)
    output_grad_A = -grad_A/F_k_t_sub # NOTE: grads hare are of log ( 1 - F_k(t)) !!!
    output_grad_v = -grad_v/F_k_t_sub
    output_grad_s = -grad_s/F_k_t_sub
    output_grad_tau = -grad_tau/F_k_t_sub

    output_grad = npj.hstack((output_grad_b, output_grad_A, output_grad_v, output_grad_s, output_grad_tau))
    #return (npj.sum(npj.log(npj.array(1-F_k_t))),output_grad)  #method 2
    return (npj.sum(npj.log(npj.array(F_k_t_sub))), output_grad)  # method 2 changed on Sep 2 to this


