#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26 15:56:11 2021
probability of sigma_alpha
added AD into this function June 10
Manual
"""
import numpy
#import jax.numpy as npj
#from jax.numpy.linalg import slogdet

import numpy as npj
from numpy.linalg import slogdet
#import scipy.io as sio
from log_multigamma import log_multigamma
#from jax import grad, value_and_grad,jit
from prior_density_hybrid import ValGrad

#import time

def p_Sigma_alpha(Sigma_alpha,v,v_a,Psi,alpha,mu_alpha,a):

    p = Psi.shape[0]
    
    Psi_inv = npj.linalg.inv(Psi)
    Sigma_alpha_inv = npj.linalg.inv(Sigma_alpha)
    
    mu_alpha=mu_alpha.reshape(-1,1)

    grad_logdet_alpha = 2*Psi_inv @(alpha-mu_alpha)
    grad_logdet_mu_alpha = -npj.sum(grad_logdet_alpha,axis=1)
    grad_logdet_log_a = 2*v_a*npj.diag(Psi_inv)/(-a.flatten())

    grad_trace_alpha = 2*Sigma_alpha_inv @ (alpha-mu_alpha)
    grad_trace_mu_alpha = -npj.sum(grad_trace_alpha,axis=1)
    grad_trace_log_a = 2*v_a*npj.diag(Sigma_alpha_inv)/(-a.flatten())
    
    grad_alpha = 0.5*v*grad_logdet_alpha - 0.5*grad_trace_alpha
    grad_mu_alpha = 0.5*v*grad_logdet_mu_alpha - 0.5*grad_trace_mu_alpha
    grad_log_a = 0.5*v*grad_logdet_log_a - 0.5*grad_trace_log_a

    output_log = 0.5*v*slogdet(Psi)[1] - 0.5*v*p*npj.log(2) - log_multigamma(v/2,p)\
                                                -0.5*(v+p+1)*slogdet(Sigma_alpha)[1] - 0.5*npj.trace(Psi @ Sigma_alpha_inv)
                                                
    output_grad = npj.concatenate([grad_alpha.flatten(order='F'), grad_mu_alpha, grad_log_a],axis=0)
    
    #print('manual')
    return ValGrad(output_log,output_grad.reshape(-1,1))



# changed to loga from a
# changed Psi to be connected to conected to parameters
# Psi is not needed as a argument here
# def p_Sigma_alpha_log(Sigma_alpha,v,v_a,Psi,alpha,mu_alpha,log_a):
#
#     J=alpha.shape[1]
#
#     a = npj.exp(log_a)
#
#
#     ## this section is added into this function on June 11
#     Psi = 2*v_a*npj.diag(1/a)
#
#     for j in range (0,J):
#           Psi = Psi + (alpha[:,j]-mu_alpha).reshape(-1,1) @ (alpha[:,j]-mu_alpha).reshape(-1,1).T
#     #####
#
#     p = Psi.shape[0]
#
#     Sigma_alpha_inv = npj.linalg.inv(Sigma_alpha)
#
#     output_log = 0.5*v*slogdet(Psi)[1] - 0.5*v*p*npj.log(2) - log_multigamma(v/2,p)\
#                                                 -0.5*(v+p+1)*slogdet(Sigma_alpha)[1] - 0.5*npj.trace(Psi @ Sigma_alpha_inv)
#
#     return npj.reshape(output_log ,())
#
#
# def p_Sigma_alpha(Sigma_alpha,v,v_a,Psi,alpha,mu_alpha,log_a):
#
#       theta_grad=value_and_grad(jit(p_Sigma_alpha_log),(4,5,6))(Sigma_alpha,v,v_a,Psi,alpha,mu_alpha,log_a) # 2 for grad theta
#       log_val=theta_grad[0]
#       grad_val= npj.hstack([theta_grad[1][0].flatten(order='F'),theta_grad[1][1].T,theta_grad[1][2].T]) # merge alpha, mu,log_a gradient
#       return ValGrad(numpy.array(log_val), numpy.array(grad_val.reshape(-1,1)))



#test 
#log_a=numpy.log(a)
#output=p_Sigma_alpha(Sigma_alpha,v,v_a,Psi,alpha,mu_alpha,log_a)

#output=p_Sigma_alpha_log(Sigma_alpha,v,v_a,Psi,alpha,mu_alpha,log_a)


# output_log=output[0]
# output_grad=output[1]
