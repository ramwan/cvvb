#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 15 22:38:27 2021
These are common LBA_HVB libraries
"""

class PriorHyperParams:
    def __init__(self, mu, cov, v_a, A_d):
        self.mu = mu
        self.cov = cov
        self.v_a = v_a
        self.A_d = A_d


# added the prior attribute to model Aug25
class Model:
    def __init__(self, name, index, num_subjects, dim, prior_par):
        self.name = name
        self.index = index
        self.num_subjects = num_subjects
        self.dim = dim
        self.prior_par = prior_par
        
class ValGrad:
    def __init__(self, val, grad):
        self.val  = val
        self.grad = grad  # array (n,1)
        
        
class LearningRateADADELTA:
    def __init__(self, v, eps):
        self.v=v
        self.eps=eps
        
        
class LearningRateADAM:
    def __init__(self, beta_1, beta_2, eps, stepsize,T):
        self.beta_1 = beta_1
        self.beta_2 = beta_2
        self.eps = eps
        self.stepsize = stepsize
        self.T = T

## Added for initialMCMC on Sep 2

class InitialMCMC:
    def __init__(self, alpha, mu, sig2, a_d):
        self.alpha = alpha
        self.mu = mu
        self.sig2 = sig2
        self.a_d = a_d

#added for generalized versoion on Aug 24

class VbSettings:
    def __init__(self,r,max_iter,max_norm,I,window,patience_par,silent,generate_samples,initial,learning_rate):
        self.r = r
        self.max_iter=max_iter
        self.max_norm = max_norm
        self.I = I
        self.window = window
        self.patience_parameter = patience_par
        self.silent = silent
        self.generate_samples = generate_samples
        self.initial = initial
        self.learning_rate = learning_rate

class Output:
    def __init__(self, lambda1, LB, LB_smooth, max_LB,skip_count,converge,theta_vb):
        
        self.lambda1 = lambda1
        self.LB = LB
        self.LB_smooth = LB_smooth
        self.max_LB = max_LB
        self.skip= skip_count
        self.converge = converge
        self.theta_vb = theta_vb
        
class Lambda1:
    def __init__(self, mu, B, d):
        self.mu = mu
        self.B = B
        self.d = d
        
        
        

