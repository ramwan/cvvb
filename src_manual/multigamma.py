#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 17:41:50 2021
This is to generate multivariate gamma 
Use this for jax compatibilty
"""
import numpy as npj
from scipy.special import gammaln
#import jax.numpy as npj
#from scipy.special import gamma#(compatible with jax,but not with jit)
#import jax
#from jax import jit
def multigamma(a,p): 
   # b=npj.array(1)
   
    #p=7  # ??input random effects needed
    #with loops
    
    # # looping is not efficient and not supported for jit , not efficient 
    # for i in range(1,p+1):
    #    # b = b*gamma(a + 0.5*(1-i)) #changed on June 25 for jit compatibility
    #     b=b*(npj.exp(jax.scipy.special.gammaln(a + 0.5*(1-i))))
   
    #max_p=20
    #gamma_val=npj.exp(jax.scipy.special.gammaln(a + 0.5*(1-npj.arange(1,p))))   #gamma values 
    #vectorized product
    
    b=npj.prod(npj.exp(gammaln(a + 0.5*(1-npj.arange(1,p)))))
    #changed to more efficient version     
    val=npj.pi**(0.25*p*(p-1)) *b
    return val


#print(multigamma(4,7))
#print(npj.exp(multigamma(4,7))) # have overflow issues.
