# -*- coding: utf-8 -*-
"""
Created on Fri May 21 9:06:52 2021
This function is to generate inverse of gamma pdf 
Same coding style as in original matlab code is used here.
"""
import numpy as npj
#import jax.numpy as npj
from scipy.special import gammaln
#import jax
# (Change to jax)


def inv_gamma_pdf(x,shape,scale):
    a = shape 
    b = scale
    
    #x= x.reshape(x.shape[0],1)
    #x= x.reshape(-1,1)
    
    #if log_value == True:
       # y = a*npj.log(b) - npj.log(gamma(a)) - (a+1)*npj.log(x) - b/x
    y = a*npj.log(b) - gammaln(a) - (a+1)*npj.log(x.reshape(-1,1)) - b/(x.reshape(-1,1))
     #changed gamma function to jax
    #else:
       # y = (b**(a)/gamma(a))* x**(-a-1)*npj.exp(-b/x)
     #   y = (b**(a)/npj.exp(jax.scipy.special.gammaln(a)))* x**(-a-1)*npj.exp(-b/x)    #changed gamma function to jax
    return y


#y=inv_gamma_pdf(x,1,1,True)