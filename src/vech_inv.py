#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu 27 9:44:23 2021
This is to generate vech(A) function
This function is generated for jax here
Need jax library

"""
import jax.numpy as jnp

def vech_inv(v,dim):
    A=jnp.ones([dim,dim])
    lower_A=jnp.tril(A) 
    ind_non_zero= jnp.transpose(jnp.array(jnp.nonzero(lower_A)))
    ind_non_zero_sorted = jnp.lexsort((ind_non_zero[:,0],ind_non_zero[:,1])) #  sort the indexes to fill by column wise first
    
    new_indexs=ind_non_zero[ind_non_zero_sorted,:]
    
    #B= np.zeros([dim,dim])
    B1=jnp.zeros([dim,dim])
    
    for i in range(new_indexs.shape[0]):
        #B[new_indexs[i,0],new_indexs[i,1]]=v[i]
        B1=jax.ops.index_update(B1,(new_indexs[i,0],new_indexs[i,1]) , v[i])
    
    return B1



#x1=vech(x)


