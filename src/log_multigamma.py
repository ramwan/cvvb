#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26 11:44:02 2021
This is to genrate the log of multi variate gamma
log_multigamma(a,p) is generated via two ways
 
Both ways are compatible for jax

"""
import jax.numpy as jnp
from jax.scipy.special import gammaln


def log_multigamma(a,p):
    j= jnp.arange(1,p+1).T
    y=p*(p-1)/4*jnp.log(jnp.pi) +jnp.sum(gammaln(a + (1-j)/2))
    return y
