# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 7:36:07 2021
This is the likelihood generation for Forstmaan set
"""
import numpy as np
from lib_lba import ValGrad,Model,PriorHyperParams
from LBA_pdf import LBA_pdf  # original matlab conversion,without jit

def matching_forstmann(model,alpha, data_RT, j, ind_acc, ind_neutral, ind_speed):
    D_alpha = np.sum(model.index)  # n_alpha = D_alpha = dimension of random effect alpha

    #Matching function1
    n_j = len(data_RT)  # number of observations of subject j

    alpha_j_1 = (np.exp(alpha[:model.index[0], j])).T  # C
    alpha_j_2 = (np.exp(alpha[model.index[0]:np.sum(model.index[:2]), j])).T  # A
    alpha_j_3 = (np.exp(alpha[np.sum(model.index[:2]):np.sum(model.index[:3]), j])).T  # v

    # check for null and remove it.
    #alpha_j_4=np.array([0])

    alpha_j_5 = (np.exp(alpha[np.sum(model.index[:4]):np.sum(model.index), j])).T  # T0

    # theta's are not equal in length(not equal trials), vectorisation is not possible then.
    # Duplicate parameter c accordingly to model
    if (model.name[0] == 3):
        M = np.hstack([ind_acc, ind_neutral, ind_speed])
        theta_j_1 = np.tile(np.sum(M * alpha_j_1, axis=1), (2, 1)).T
    elif (model.name[0] == 2):  # check here
        M = np.hstack([ind_acc + ind_neutral, ind_speed])
        theta_j_1 = np.tile(np.sum(M * alpha_j_1, axis=1), (2, 1)).T
    else:
        theta_j_1 = np.tile(alpha_j_1, (n_j, 2))

    #             # Duplicate parameter A accordingly to model
    theta_j_2 = np.tile(alpha_j_2, (n_j, 2))

    # Duplicate parameter v accordingly to model
    if (model.name[2] == 3):  # not working here in matlab code
        theta_j_3 = ind_acc * alpha_j_3[:2] + ind_neutral * alpha_j_3[2:4] + ind_speed * alpha_j_3[4:6]
    elif (model.name[2] == 2):  # check here for model 2, not working
        theta_j_3 = (ind_acc + ind_neutral) * alpha_j_3[:2] + ind_speed * alpha_j_3[2:4]
    else:
        theta_j_3 = np.tile(alpha_j_3, (n_j, 1))

    # Duplicate parameter sv accordingly to model
    theta_j_4 = np.ones([n_j, 2])

    # Duplicate parameter tau accordingly to model
    if (model.name[4] == 3):
        #M = np.vstack([ind_acc, ind_neutral, ind_speed]).T
        M = np.hstack([ind_acc, ind_neutral, ind_speed])
        theta_j_5 = np.tile(np.sum(M * alpha_j_5 , axis=1), (2, 1)).T #Changed on Aug 31
    elif (model.name[4] == 2):
        M = np.hstack([ind_acc + ind_neutral, ind_speed])
        theta_j_5 = np.tile(np.sum(M * alpha_j_5 , axis=1), (2, 1)).T #Changed on Aug 31
    else:
        theta_j_5 = np.tile(alpha_j_5, (n_j, 2))

    theta_j_1 = theta_j_1 + theta_j_2  # Convert theta_j_1 from C to B.
    alpha_j_1 = alpha_j_1 + alpha_j_2  # Convert alpha_j_1 from C to B.

    # return of alpha variables is unused
    return  [theta_j_1,theta_j_2,theta_j_3,theta_j_4,theta_j_5]#,alpha_j_1,alpha_j_2,alpha_j_3,alpha_j_4,alpha_j_5]


def matching_gradients_forstmann(model,LBA_j_grad,z_j, ind_acc, ind_neutral, ind_speed):
    LBA_j_grad_b = LBA_j_grad[:, :2]
    LBA_j_grad_A = LBA_j_grad[:, 2:4]
    LBA_j_grad_v = LBA_j_grad[:, 4:6]
    LBA_j_grad_tau = LBA_j_grad[:, 8:10]


    #added this Aug20
    theta_j_1 = z_j[0]
    theta_j_2 = z_j[1]
    theta_j_3 = z_j[2]
    theta_j_5 = z_j[4]

    # # Multiply by the Jacobian to get gradient wrt to alpha
    grad_LBA_j_1 = LBA_j_grad_b * (theta_j_1 - theta_j_2)  # grad_alpha_c
    grad_LBA_j_2 = (LBA_j_grad_b + LBA_j_grad_A) * theta_j_2  # grad_alpha_A
    grad_LBA_j_3 = LBA_j_grad_v * theta_j_3  # grad_alpha_v
    grad_LBA_j_5 = LBA_j_grad_tau * theta_j_5  # grad_alpha_tau

    # Rearranage the gradient of log_pdf of alpha_c
    if (model.name[0] == 3):
        grad_alpha_j_1 = np.vstack(
            [np.sum(grad_LBA_j_1 * ind_acc), np.sum(grad_LBA_j_1 * ind_neutral), np.sum(grad_LBA_j_1 * ind_speed)])
    elif (model.name[0] == 2):
        grad_alpha_j_1 = np.vstack([np.sum(grad_LBA_j_1 * (ind_acc + ind_neutral)), np.sum(grad_LBA_j_1 * ind_speed)])
    else:
        grad_alpha_j_1 = np.sum(grad_LBA_j_1 * (ind_acc + ind_neutral + ind_speed))

        # Rearranage the gradient of log_pdf of alpha_A

    grad_alpha_j_2 = np.sum(grad_LBA_j_2)

    # Rearranage the gradient of log_pdf of alpha_v
    if (model.name[2] == 3):
        grad_alpha_j_3 = np.hstack([np.sum(grad_LBA_j_3 * ind_acc, axis=0), np.sum(grad_LBA_j_3 * ind_neutral, axis=0),
                                    np.sum(grad_LBA_j_3 * ind_speed, axis=0)]).reshape([-1, 1])
    elif (model.name[2] == 2):
        grad_alpha_j_3 = np.hstack(
            [np.sum(grad_LBA_j_3 * (ind_acc + ind_neutral), axis=0), np.sum(grad_LBA_j_3 * ind_speed, axis=0)]).reshape(
            [-1, 1])
    else:
        grad_alpha_j_3 = np.sum(grad_LBA_j_3 * (ind_acc + ind_neutral + ind_speed), axis=0).reshape([-1, 1])

        # Rearranage the gradient of log_pdf of alpha_s

    # Rearranage the gradient of log_pdf of alpha_tau

    if (model.name[4] == 3):
        grad_alpha_j_5 = np.vstack(
            [np.sum(grad_LBA_j_5 * ind_acc), np.sum(grad_LBA_j_5 * ind_neutral), np.sum(grad_LBA_j_5 * ind_speed)])
    elif (model.name[4] == 2):
        grad_alpha_j_5 = np.vstack([np.sum(grad_LBA_j_5 * (ind_acc + ind_neutral)), np.sum(grad_LBA_j_5 * ind_speed)])
    else:
        grad_alpha_j_5 = np.sum(grad_LBA_j_5 * (ind_acc + ind_neutral + ind_speed))

    return np.vstack([grad_alpha_j_1.reshape(-1, 1)[:], grad_alpha_j_2.reshape(-1, 1)[:], grad_alpha_j_3.reshape(-1, 1)[:],
                   grad_alpha_j_5.reshape(-1, 1)[:]]).T

def likelihood_Hybrid(model,alpha,data,gradient):
    data_RE = data['RE']
    data_RT = data['RT']
    data_E = data['E']

    loga = 0 # log of the total likelihood
    
    D_alpha = int( np.sum(model.index) )# n_alpha = D_alpha = dimension of random effect alpha
    J = len(data_RT) # number of subjects/participants 
    grad_alpha = np.zeros([D_alpha,J]) # store all the gradients wrt alpha_j
    grad_mu = np.zeros([D_alpha,1]) 
    grad_loga = np.zeros([D_alpha,1]) 

    sizes = [len(data_RE[j]) for j in range(J)]

    ind_acc = [(data_E[j] == 1).reshape(-1, 1) for j in range(J)]
    ind_neutral = [(data_E[j] == 2).reshape(-1, 1) for j in range(J)]
    ind_speed = [(data_E[j] == 3).reshape(-1, 1) for j in range(J)]

    RE_list = [data_RE[i] for i in range(J)]
    RT_list = [data_RT[i] for i in range(J)]
    total_RE = np.concatenate(RE_list)
    total_RT = np.concatenate(RT_list)

    total_b = None
    total_A = None
    total_v = None
    total_s = None
    total_tau = None
    
    for j in range(J):
        m1_temp = matching_forstmann(model, alpha, RT_list[j], j, ind_acc[j], ind_neutral[j], ind_speed[j])
        if j == 0:
            total_b = m1_temp[0]
            total_A = m1_temp[1]
            total_v = m1_temp[2]
            total_s = m1_temp[3]
            total_tau = m1_temp[4]
        else:
            total_b = np.vstack((total_b, m1_temp[0]))
            total_A = np.vstack((total_A, m1_temp[1]))
            total_v = np.vstack((total_v, m1_temp[2]))
            total_s = np.vstack((total_s, m1_temp[3]))
            total_tau = np.vstack((total_tau, m1_temp[4]))

    # we should expect a return type of shape ((J, 1), (J, sum(sizes) * 10))
    total_LBA = LBA_pdf(total_RE, total_RT, total_b, total_A, total_v, \
                           total_s, total_tau, gradient, sizes)
    # without dtype=double, we'll have ~1e-5 magnitude difference in value
    loga = np.sum(total_LBA[0], dtype=np.double)

    if gradient:
        pre = 0
        for j in range(J):
            LBA_j_grad = total_LBA[1][j]
            m1_temp = [ total_b[pre:pre+sizes[j]],\
                        total_A[pre:pre+sizes[j]],\
                        total_v[pre:pre+sizes[j]],\
                        total_s[pre:pre+sizes[j]],\
                        total_tau[pre:pre+sizes[j]] ]
            pre += sizes[j]

            grad_alpha[:, j] = matching_gradients_forstmann(model, LBA_j_grad, m1_temp,\
                                                            ind_acc[j], ind_neutral[j], ind_speed[j])

    ## output of the function
    if gradient == True:
        grad_val = np.vstack([grad_alpha.flatten(order='F').reshape(-1, 1), grad_mu, grad_loga])
        return ValGrad(loga, np.array(grad_val))
    else:
        return ValGrad(np.array(loga), np.array([0]))
