import copy, os, pickle, sys, time
import multiprocessing as mp
import numpy as np
import scipy.io as sio

from Hybrid_VAFC import Hybrid_VAFC
from likelihood_hybrid import likelihood_Hybrid
from lib_lba import Model,PriorHyperParams,LearningRateADADELTA,LearningRateADAM,Output,InitialMCMC,VbSettings,Lambda1
from ModelSpecification import model_specification
from sklearn.model_selection import KFold
from vb_adaptive_initialization import vb_adaptive_initialization
from vech import vech

help_option = ["-h", "--help", "help", "h"]

#VB Tuning parameters
#max_iter = 10  # number of iterations - 5000 at least
max_iter = 10000
max_norm = 1000  # upper bound the the norm of the gradients

K = 5 # no of folds
#r = 1  # B has size p x r - at least 10
r = 15
I = 10  # number of Monte Carlo samples to estimate the gradients
#window = 5 # 100 
window = 100
#patience_parameter = 20
patience_parameter = 50
a_t = 0.9  # temperature for annealing

def run_model(m, model, data, dmat, numProcessors):
    silent = True # False #display estimates for at each iteration
    generate_samples = False
    learning_rate = LearningRateADADELTA(0.95, 10 ** (-7))
    lambda1 = Lambda1(np.array([0]) , np.array([0]) ,np.array([0]))
    vb_settings = VbSettings(r, max_iter, max_norm, I, window, patience_parameter, silent, generate_samples, lambda1,learning_rate)

    # step2 randomly partition the data in Kfolds
    J = len(data['RE'])
    kf = KFold(n_splits=K, random_state=0, shuffle=True)
    data_fields = list(data.keys())

    train_ind = []
    test_ind = []
    for j in range(J):
        RT = data['RT'][j]
        train_j = [] #save subject train index for K folds
        test_j = []  #save subject train index for K folds

        #loop for K folds
        for train_index, test_index in kf.split(RT):
            train_j.append(train_index)
            test_j.append(test_index)

        train_ind.append(train_j)
        test_ind.append(test_j)

    #Step 3: Kfold CVVB algorithm
    # expected log predictive density
    elpd_cvvb = np.zeros([K]) #store all ELPD estimates

    time1 = time.time()
    # Also same as D_latent, number of random effects per participant
    D_alpha = int(np.sum(model[m].index)) 
    p = D_alpha * (J + 2)  # dim(theta) = p

    model_name=(' c ~'+ str(model[m].name[0]) + '| A ~ '+ str(model[m].name[1]) + '| v ~ ' + str( model[m].name[2]) + '| s ~ '+ str(model[m].name[3]) +'| t0 ~ '+ str(model[m].name[4])  )
    print('Model '+ str(m+1)+ str(model_name))

    #CVVB for the current model
    k=0
    vb_results_model_m= [] #save VB results of model m

    # if for checking 27 models of Forstsmann using matlab intial values as CVVB experiment
    lambda_mu = dmat['initial_MCMC_Forstmann'][m][0].flatten() # ??

    lambda_B = np.zeros([p, r]) / r
    lambda_B = np.tril(lambda_B)
    lambda_d = 0.01 * np.ones([p, 1])
    
    # properly set the lambda parameter
    lambda1 = Lambda1(lambda_mu, lambda_B, lambda_d)

    while k < K:
        # partition our data according to the k-fold splitting
        train_data = {f: [] for f in data_fields}
        test_data = {f: [] for f in data_fields}

        for field in data_fields:
            for j in range (J):
                field_data = data[field][j]
                train_data[field].append(field_data[train_ind[j][k]])
                test_data[field].append(field_data[test_ind[j][k]])

            train_data[field] = np.array(train_data[field], dtype=object)
            test_data[field] = np.array(test_data[field], dtype=object)

        vb_settings.initial = lambda1
        t1 = time.time_ns()
        output = Hybrid_VAFC(model[m], train_data, vb_settings, numProcessors)
        t2 = time.time_ns()
        print("Fold %d time: %f ms" %(k, (t2-t1)/1000000))
        lambda1 = output.lambda1
        vb_results_model_m.append(output)

        #Compute the Kfold CVVB ELPD Estimation
        if (output.converge == True) | (output.converge == 'reach_max_iter'):
            mu = lambda1.mu
            B = lambda1.B
            d = lambda1.d

            N = 10  # number of samples to estimate ELPD
            log_pdfs = np.zeros([N,1])
            model_current = model[m]
            for n in range(0, N):
                epsilon = np.random.randn(p, 1)
                z = np.random.randn(r, 1)
                
                # theta_1 = (alpha_1,...,alpha_J,mu_alpha,log a_1,...,log a_D)
                theta_1 = (mu.reshape(-1, 1) + B @ z + d.reshape(-1,1) * epsilon).squeeze()  

                alpha = np.reshape(theta_1[:D_alpha * J], [D_alpha, J], order='F')
                like = likelihood_Hybrid(model_current, alpha, test_data, False)
                log_pdfs[n] = like.val   #get loglikelihood

            max_log = np.max(log_pdfs)
            pdfs_ratio=np.exp(log_pdfs-max_log)
            elpd_cvvb[k] = max_log + np.log(np.mean(pdfs_ratio))

            print(['Fold' + str(k) +' || ELPD = ' + str(np.round(elpd_cvvb[k])) + ' || Initial LB = ' + str (output.LB[0]) +
                   ' || max LB = ' + str (output.max_LB) + ' || N_iter = ' + str (len(output.LB))])
            k=k+1
        else:
            print('This model is not well approximated, skip')
            k= K+1 # to skip next folds for same model

    #save model m all cross validation vb results
    time2 = time.time()

    print(['K fold CVVB estimate of ELPD = ' + str(np.mean(elpd_cvvb[:]))+ ', K fold run time ' + str((time2-time1)/60) + ' minutes'])

    total_time = time2 - time1
    return (m, total_time, vb_results_model_m, elpd_cvvb)


if __name__=='__main__':
    numProcessors = 1
    if len(sys.argv) > 1:
        if sys.argv[1] in help_option:
            print("Usage:    python3 *exec_name*.py [num processors (int)]")
            print("For help: python3 *exec_name*.py -h")
            sys.exit(0)
        try:
            numProcessors = int(sys.argv[1])
        except:
            print("Invalid number of processors given to program, please specify integer.")
            sys.exit(1)


    #step0 data preparation
    exp_name = 'Forstmann'
    dataset_name='Forstmann.mat'
    # set the path accordingly to your pc path/HPC path
    dataset_path = os.path.join("./", dataset_name) #pc

    # load 27 models initial
    #set the path accordingly to your pc path/HPC path
    initial_path_models = os.path.join("./", 'intial_values_forstmann.mat') #hpc
    M,model = model_specification('Forstmann')

    dmat= sio.loadmat(initial_path_models) # initial path models
    mat = sio.loadmat(dataset_path)  # load mat-file
    mdata = mat['data']  # variable in mat file
    mdtype = mdata.dtype  # dtypes of structures are "unsized objects"

    data = {}
    for n in mdtype.names:
        data[n] = np.array([np.squeeze(mdata[n][0, 0][m, 0])\
                               for m in range(len(mdata[n][0, 0])) ],\
                           dtype=object)

    with mp.Pool(processes=numProcessors) as p:
        runs = [p.apply_async(run_model, (m, model, data, dmat, 1))\
               for m in range(1)] # M
        results = [run.get() for run in runs]
        #runs = [p.apply_async(run_model, (5, model, dataset_path, initial_path_models))]
        #results = [run.get() for run in runs]
    #for m in range(M):
    #    run_model(0, model, data, dmat, numProcessors)

    f = open('output_cvvb_Sep13_w100_p20_i200.pickle', 'wb')
    #pickle.dump([model,vb_results,elpd_cvvb], f)
    pickle.dump(results, f)
    #pickle.dump([model,prior_par,initial_MCMC['initial'],output], f)
    f.close()

    total_time = 0
    for r in results:
        total_time += r[1]

    print(['total run time ' + str(total_time/60) +' minutes'])
