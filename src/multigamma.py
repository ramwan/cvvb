#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 17:41:50 2021
This is to generate multivariate gamma 
Use this for jax compatibilty
"""
import jax.numpy as jnp
from jax.scipy.special import gammaln
from jax import jit
from functools import partial

def multigamma(a,p): 
   # b=np.array(1)
   
    #p=7  # ??input random effects needed
    #with loops
    
    # # looping is not efficient and not supported for jit , not efficient 
    # for i in range(1,p+1):
    #    # b = b*gamma(a + 0.5*(1-i)) #changed on June 25 for jit compatibility
    #     b=b*(np.exp(jax.scipy.special.gammaln(a + 0.5*(1-i))))
   
    #max_p=20
    #gamma_val=np.exp(jax.scipy.special.gammaln(a + 0.5*(1-np.arange(1,p))))   #gamma values 
    #vectorized product
    
    b = jnp.prod(jnp.exp(gammaln(a + 0.5*(1-jnp.arange(1,p)))))
    #changed to more efficient version     
    val = jnp.power(jnp.pi, (0.25*p*(p-1))) * b
    return val

def multigamma_jit(array_p, a):
    p = len(array_p) + 1
    b = jnp.prod(jnp.exp(gammaln(a + 0.5*(1-array_p))))
    val = jnp.power(jnp.pi, (0.25*p*(p-1))) * b
    return val
