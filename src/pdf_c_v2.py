import jax.numpy as jnp
from jax.scipy.stats import norm
from jax import value_and_grad, vmap, jit

def pdf_c(t,b,A,v,s,tau):
    w1 = lambda b1, t1, v1, s1, tau1: jnp.divide((b1 - jnp.multiply(t1 - tau1, v1)), jnp.multiply(s1, t1 - tau1))
    w2 = lambda A1, s1, t1, tau1: jnp.divide(A1, jnp.multiply(s1, t1 - tau1))

    x = lambda b1, t1, v1, s1, A1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    x1 = lambda b1, t1, v1, s1, A1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1) - w2(A1, s1, t1, tau1))
    z = lambda b1, t1, v1, s1, tau1: norm.cdf(w1(b1, t1, v1, s1, tau1))
    z1 = lambda b1, t1, v1, s1, tau1: norm.pdf(w1(b1, t1, v1, s1, tau1))

    eps = 10e-4
    sigma = 10e-3
    x_trans = lambda b1, t1, v1, s1, A1, tau1: 2*x(b1,t1,v1,s1,A1,tau1) - 1
    z_trans = lambda b1, t1, v1, s1, tau1: 2*z(b1,t1,v1,s1,tau1) - 1
    xy = lambda b1, t1, v1, s1, A1, tau1:\
            jnp.multiply(x_trans(b1,t1,v1,s1,A1,tau1) - eps, \
                norm.cdf(jnp.divide(x_trans(b1,t1,v1,s1,A1,tau1), sigma))) + \
            jnp.multiply(x_trans(b1,t1,v1,s1,A1,tau1) - eps, \
                (1 - norm.cdf(jnp.divide(x_trans(b1,t1,v1,s1,A1,tau1), sigma))))
    zy = lambda b1, t1, v1, s1, tau1:\
            jnp.multiply(z_trans(b1,t1,v1,s1,tau1) - eps,  \
                norm.cdf(jnp.divide(z_trans(b1,t1,v1,s1,tau1), sigma))) + \
            jnp.multiply(z_trans(b1,t1,v1,s1,tau1) - eps, \
                (1 - norm.cdf(jnp.divide(z_trans(b1,t1,v1,s1,tau1), sigma))))
    xyy = lambda b1, t1, v1, s1, A1, tau1:\
            jnp.divide(xy(b1,t1,v1,s1,A1,tau1) + 1, 2)
    zyy = lambda b1, t1, v1, s1, tau1:\
            jnp.divide(zy(b1,t1,v1,s1,tau1) + 1, 2)

    f_c_t = lambda b1, A1, v1, s1, t1, tau1: \
        jnp.sum(jnp.log(jnp.divide(
            jnp.multiply(v1, zyy(b1, t1, v1, s1, tau1) - xyy(b1, t1, v1, s1, A1, tau1))\
            + jnp.multiply(s1, x1(b1, t1, v1, s1, A1, tau1) - z1(b1, t1, v1, s1, tau1))\
            , A1)
        ))

    output_grad = value_and_grad(f_c_t, (0, 1, 2, 3, 5))
    (val, grads) = vmap(output_grad)(b, A, v, s, t, tau)
    grads = jnp.vstack(grads)

    return (val, grads)
