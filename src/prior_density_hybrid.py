# -*- coding: utf-8 -*-
"""
Created on Mon May 31 7:14:57 2021
This function does the prior density in the hybrid LBA method 
"""

import jax.numpy as jnp
import jax.numpy.linalg as jnl
from multigamma import multigamma, multigamma_jit
from inv_gamma_pdf import inv_gamma_pdf, inv_gamma_pdf_jit
from jax import value_and_grad,jit, local_devices
from lib_lba import ValGrad,Model,PriorHyperParams
from functools import partial

import time
import os


## This function is for derived log prior
#had to change this to loga    
#@partial(jit, static_argnums=3)
def obtain_log_prior_VAFC_LBA_v2(D_alpha, prior_par_v_a, prior_par_A_d,\
                                 J, alpha, mu_alpha, log_a, Sigma_alpha):
    
    J = jnp.array(J)
    v = prior_par_v_a+ D_alpha - 1

    print(v)
    print(D_alpha)
    
    Sigma_alpha_inv = lambda sa1: jnp.linalg.inv(sa1)
    A = lambda sa1, alpha1, mu1:\
        Sigma_alpha_inv(sa1) @ (alpha1 - (jnp.array([mu1.flatten()] * alpha1.shape[1]).T))
    log_prior = lambda alpha1, mu1, loga1, sa1:\
        - 0.5*D_alpha*(J+1)*jnp.log(2*jnp.pi)\
        - 0.5*(J + v + D_alpha +1)*jnp.linalg.slogdet(sa1)[1]\
        - 0.5*jnp.trace((alpha1-(jnp.array([mu1.flatten() ]*alpha1.shape[1]).T)).T \
            @ A(sa1, alpha1, mu1))\
        - 0.5*(mu1.T @ mu1) \
        + 0.5* v * (D_alpha* jnp.log(2*prior_par_v_a) - jnp.sum(loga1))\
        - 0.5*v*D_alpha*jnp.log(2) \
        - jnp.log(multigamma(v/2, D_alpha))\
        - prior_par_v_a * jnp.sum(jnp.diag(Sigma_alpha_inv(sa1))/jnp.exp(loga1)) \
        + jnp.sum(inv_gamma_pdf(jnp.exp(loga1), 0.5*jnp.ones([D_alpha,1]), 1 /(prior_par_A_d**2)))\
        + jnp.sum(loga1) # log Jacobian

    output_grad = value_and_grad(log_prior, (0, 1, 2))
    (val, [grad_alpha, grad_mu, grad_loga]) = output_grad(alpha, mu_alpha, log_a, Sigma_alpha)

    grad_alpha = jnp.stack(grad_alpha, axis=1).flatten()
    prior_grad=jnp.vstack((grad_alpha.reshape(-1,1),grad_mu.reshape(-1,1),grad_loga.reshape(-1,1)))

    return (val,prior_grad)
 
def prior_density_hybrid(model,alpha,mu_alpha,log_a,Sigma_alpha,prior_par):
    D_alpha= jnp.sum(model.index)# random affects /per participants
    J=model.num_subjects # no of participants

    prior_par_v_a=jnp.array(prior_par.v_a)
    prior_par_A_d=prior_par.A_d

    theta_grad2 = obtain_log_prior_VAFC_LBA_v2(D_alpha, prior_par_v_a, prior_par_A_d,\
                                               J, alpha, mu_alpha, log_a, Sigma_alpha)
    return ValGrad(theta_grad2[0], theta_grad2[1])   #convert to numpy array
