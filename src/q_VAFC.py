# -*- coding: utf-8 -*-
"""
Created on Fri May 20 13:13:42 2021

Can we include this into the automatic AD part??
Modified on Jun 8 included AD.
"""

import jax.numpy as jnp
from jax import value_and_grad,jit
from lib_lba import ValGrad
from time import perf_counter

@jit
def y_log_ad(theta, mu, B, d):
    [m, p] = B.shape
    covmat = B@B.T + jnp.diagflat(d**2)
    term = theta - mu
    return -0.5*p*jnp.log(2*jnp.pi) - 0.5*jnp.linalg.slogdet(covmat)[1] \
                - 0.5*term.T @ jnp.linalg.inv(covmat) @ term

def q_VAFC_ad(theta, mu, B, d):
    output_grad = value_and_grad(y_log_ad, (0))
    (val, grads) = output_grad(theta, mu, B, d)
    return ValGrad(val, grads.reshape(-1, 1))


def q_VAFC_og(theta,mu,B,d):
    # NOTE: This version is the same as q_gaussian_factor_covariance.m, except
    # that the input is (mu,B,d), not lambda as in the latter
    # INPUT: lambda = (mu,B,d), theta ~ N(mu,covmat)
    #                 with covmat = lambda.B*lambda.B' + diag(lambda.d.^2);
    # OUTPUT: y_log = log (q_lambda)
    #         y_theta = inv(BB'+D^2)*(Bz + d.*eps) %gradient of q wrt beta

    [m, p] = B.shape
    covmat = B@B.T + jnp.diagflat(d**2)
    term = theta - mu
    D_inv2 = jnp.diagflat(d**(-2))

    precision_matrix = D_inv2 - D_inv2 @ B @jnp.linalg.inv(jnp.eye(p) + B.T @ D_inv2 @ B)@ B.T@ D_inv2

   # y_log = -0.5*p*np.log(2*pi) - 0.5*logdet(covmat) - 0.5*term.T/covmat*term
    y_log = -0.5*p*jnp.log(2*jnp.pi) - 0.5*jnp.linalg.slogdet(covmat)[1] - 0.5*term.T @ jnp.linalg.inv(covmat) @term

    y_grad = - precision_matrix @term

    return ValGrad(y_log,y_grad.reshape(-1,1))

def q_VAFC(theta, mu, B, d):
    #t1 = perf_counter()
    #og = q_VAFC_og(theta, mu, B, d)
    #t2 = perf_counter()
    ad = q_VAFC_ad(theta, mu, B, d)
    #t3 = perf_counter()

    """ Testing block
    print("val diff: ", jnp.abs(og.val - ad.val))
    diff = og.grad.flatten() - ad.grad.flatten()
    print("grad diff: ", jnp.abs(jnp.dot(diff, diff)))
    print("manual time (ms):", (t2-t1)*1000)
    print("ad time (ms): ", (t3-t2)*1000)
    """

    return ad
