#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26 15:56:11 2021
probability of sigma_alpha
added AD into this function June 10
Manual
"""
import jax.numpy as jnp
from jax import value_and_grad,jit
from jax.numpy.linalg import slogdet
from log_multigamma import log_multigamma
from prior_density_hybrid import ValGrad
from time import perf_counter

"""
We note here that Sigma_alpha actually depends on 'a' but jax doesn't have
an invwishart function meaning we can't use autodiff on it. The grad diff for 
log_a doesn't vary too much from expected so for now it's ok....
"""
@jit
def output_log_ad(Sigma_alpha, v, v_a, alpha, mu_alpha, a):
    Psi = 2 * v_a * jnp.diag(1/jnp.exp(a))
    for j in range(0, len(alpha[0])):
        Psi = Psi + (alpha[:,j] - mu_alpha).reshape(-1,1) @ (alpha[:,j] - mu_alpha).reshape(-1,1).T

    p = Psi.shape[0]
    Sigma_alpha_inv = jnp.linalg.inv(Sigma_alpha)
    return 0.5*v*slogdet(Psi)[1] - 0.5*v*p*jnp.log(2) - log_multigamma(v/2, p) \
                - 0.5*(v+p+1)*slogdet(Sigma_alpha)[1] - 0.5*jnp.trace(Psi @ Sigma_alpha_inv)

def p_Sigma_alpha(Sigma_alpha, v, v_a, alpha, mu_alpha, a):
    output_log_ad(Sigma_alpha, v, v_a, alpha, mu_alpha, a)
    output_grad = value_and_grad(output_log_ad, (3, 4, 5))
    (val, grad) = output_grad(Sigma_alpha, v, v_a, alpha, mu_alpha, a)

    grad = jnp.concatenate([grad[0].flatten(order='F'), grad[1], grad[2]], axis=0)
    return ValGrad(val, grad.reshape(-1, 1))
