class Person:
    def __init__(self,name,age):
        self.name=name
        self.age=age

    def print_name(self):
        print('Hello name '+ self.name)


class Child(Person):
    def __init__(self,name,age,grad):
        Person.__init__(self,name,age)
        self.grad = grad

def lin_search(list1,val):
    for i in range(len(list1)):
        if list1[i]==val:
            return True
    return -1

def bin_search(list1,val):

    low = 0
    high = len(list1)-1


    while (low <= high):
        mid = (low + high) // 2
        if (list1[mid]==val):
            return 1
        elif (val> list1[mid]): # go to right halve
            low=mid+1
        else: # go to left halve
            High=mid -1

    if (low > high):
        return 0



if __name__== '__main__':
    # p1=Person('A',1)
    # p2=Person ('B',2)
    #
    # c1=Child('A',1,'2000')
    #
    # print(c1.grad)
    # p1.print_name()
    #
    #
    # x=lambda a,b,c: a+b+c
    # print(x(1,2,3))
    # print(11//3)

    list1=[101,2,3,4,5,101]
    set1={1,3,4,3,3,3,0,2}
    # list1 = list(range(10))
    # print(list1.count(999))
    # print(lin_search(list1,999))
    # print(bin_search(list1, 999))

    set1.pop()
    print(set1)