# -*- coding: utf-8 -*-
"""
Created on Thu Aug 28 2021
This is the likelihood generation for Forstmaan set
This uses not jit functions, and makes the code runnable without jax
But 3.5 to 4 times slower the Matlab manual.

"""
import numpy as np
#from LBA_pdf import LBA_pdf
from LBA_pdf_v2 import LBA_pdf_v2
from lib_lba import ValGrad,Model,PriorHyperParams
import multiprocessing
import time

#load data
# mat = sio.loadmat('Forstmann.mat')  # load mat-file
# mdata = mat['data']  # variable in mat file
# mdtype = mdata.dtype  # dtypes of structures are "unsized objects"
# data = {n: mdata[n][0, 0] for n in mdtype.names}


# class Model:
#     def __init__(self, name, index, num_subjects, dim):
#         self.name = name
#         self.index = index
#         self.num_subjects = num_subjects
#         self.dim = dim
        
#model=Model(np.array([3,1,1,0,1]),np.array([3,1,2,0,1]),19,147)

# model=Model(np.array([2,1,1,0,1]),np.array([2,1,2,0,1]),19,126)

# model=Model(np.array([1,1,1,0,1]),np.array([1,1,2,0,1]),19,105)

# model=Model(np.array([3,1,2,0,1]),np.array([3,1,4,0,1]),19,189)

# model=Model(np.array([3,1,3,0,1]),np.array([3,1,6,0,1]),19,231)
#gradient=True



def likelihood_Forstmann_Hybrid(model,alpha,data,gradient):

    data_RE=data['RE']  #data_RE_11=data_RE[j-1][0] to access the J=1
    data_RT=data['RT']
    data_E=data['E']

    loga = 0; # log of the total likelihood
    
    D_alpha = np.sum(model.index) # n_alpha = D_alpha = dimension of random effect alpha
    J = len(data_RT) # number of subjects/participants
    # alpha = reshape(theta(1:D_alpha*J),D_alpha,J); # this matrix store all the random effects
    
    grad_alpha = np.zeros([D_alpha,J]) # store all the gradients wrt alpha_j
    grad_mu = np.zeros([D_alpha,1]) 
    grad_loga = np.zeros([D_alpha,1]) 

    
    for j in range(0,J): 
        
        #start= time.time()
    ## Match each observation to the correct set of parameters (b,A,v,s,tau)
        n_j = len(data_RT[j][0]) # number of observations of subject j
        
        #data_E=data_E[j][0] ##added this to get data_E values per subject
        
        ind_acc = (data_E[j][0] == 1)   
        ind_neutral = (data_E[j][0] == 2)     
        ind_speed = (data_E[j][0] == 3)    
        
        
        alpha_j_1 = (np.exp(alpha[:model.index[0],j])).T # C
        alpha_j_2 = (np.exp(alpha[model.index[0]:np.sum(model.index[:2]),j])).T # A
        alpha_j_3 = (np.exp(alpha[np.sum(model.index[:2]):np.sum(model.index[:3]),j])).T # v
        
        #alpha_j_4 = np.array([(alpha[np.sum(model.index[:3]):np.sum(model.index[:4]),j]).T, 1]) # S
        
        #check for null and remove it.
        #alpha_j_4=np.array(list(filter(None, alpha_j_4)),dtype='float64')
        
        alpha_j_5 = (np.exp(alpha[np.sum(model.index[:4]):np.sum(model.index),j])).T # T0
             
        #theta's are not equal in length(not equal trials), vectorisation is not possible then.
                # Duplicate parameter c accordingly to model
        if (model.name[0]== 3): 
            M = np.hstack([ind_acc, ind_neutral ,ind_speed])
            theta_j_1 = np.tile(np.sum(M*alpha_j_1, axis=1),(2,1)).T
        elif (model.name[0]== 2): # check here
            M = np.hstack([ind_acc+ ind_neutral ,ind_speed])
            theta_j_1 = np.tile(np.sum(M*alpha_j_1, axis=1),(2,1)).T
        else:
            theta_j_1 = np.tile(alpha_j_1,(n_j,2))
    
    #             # Duplicate parameter A accordingly to model
        theta_j_2 = np.tile(alpha_j_2,(n_j,2))
        
                # Duplicate parameter v accordingly to model
        if (model.name[2] == 3): #not working here in matlab code
           theta_j_3 = ind_acc*alpha_j_3[:2] +  ind_neutral*alpha_j_3[2:4]+ ind_speed*alpha_j_3[4:6]
        elif (model.name[2] == 2): #check here for model 2, not working
            theta_j_3 = (ind_acc + ind_neutral)*alpha_j_3[:2] + ind_speed*alpha_j_3[2:4]
        else:
            theta_j_3 = np.tile(alpha_j_3,(n_j,1))
    
    
    #             # Duplicate parameter sv accordingly to model
        theta_j_4 = np.ones([n_j,2])
        
                # Duplicate parameter tau accordingly to model
        if (model.name[4]== 3): 
            M = np.hstack([ind_acc, ind_neutral ,ind_speed])
            theta_j_5 = np.tile(M*alpha_j_5,(1,2))
        elif (model.name[4]== 2): 
            M = np.hstack([ind_acc+ ind_neutral ,ind_speed])
            theta_j_5 = np.tile(M*alpha_j_5,(1,2))
         #   pass
        else:
            theta_j_5 = np.tile(alpha_j_5,(n_j,2))
        
        
        theta_j_1 = theta_j_1 + theta_j_2 # Convert theta_j{1} from C to B.
        
    ## Compute the log and gradients # dimention is number of trials x 10
        start= time.time()
        LBA_j = LBA_pdf(data_RE[j],data_RT[j],theta_j_1,theta_j_2,theta_j_3,theta_j_4,theta_j_5,gradient) #200ms/ jit 6.82ms/
                                                                                                                # man 2ms /
        #                                                                                                       #man jit 1.1ms/
                                                                                                                # after join bavtu 0.7ms
        loga = loga + LBA_j[0]

        
        #time1=time.time()
        #print(f"Runtime of the program 127 is {time1 - start}")
        
        if gradient == True:
            
            #get gradients respect b,A,v,s, tau
            LBA_j_grad=LBA_j[1]
            LBA_j_grad_b=LBA_j_grad[:,:2]
            LBA_j_grad_A=LBA_j_grad[:,2:4]
            LBA_j_grad_v=LBA_j_grad[:,4:6]
            #LBA_grad_s=LBA_j_grad[:,6:8]
            LBA_j_grad_tau=LBA_j_grad[:,8:10]
        
    # # Multiply by the Jacobian to get gradient wrt to alpha   
            grad_LBA_j_1 = LBA_j_grad_b*(theta_j_1-theta_j_2) # grad_alpha_c
            grad_LBA_j_2 = (LBA_j_grad_b + LBA_j_grad_A)*theta_j_2# grad_alpha_A
            grad_LBA_j_3 = LBA_j_grad_v*theta_j_3 #grad_alpha_v 
    #         grad_LBA_j_4 = LBA_j_grad_s*theta_j_4; % grad_alpha_s
            grad_LBA_j_5 = LBA_j_grad_tau*theta_j_5 # grad_alpha_tau
        
    # Rearrange the gradients into correct positions
            #grad_alpha_j = cell(1,5);
            
                    # Rearranage the gradient of log_pdf of alpha_c
                    
            if (model.name[0]== 3): 
                grad_alpha_j_1 = np.vstack([np.sum(grad_LBA_j_1*ind_acc), np.sum(grad_LBA_j_1*ind_neutral), np.sum(grad_LBA_j_1*ind_speed)])
            elif (model.name[0]== 2): 
                grad_alpha_j_1 = np.vstack([np.sum(grad_LBA_j_1*(ind_acc+ind_neutral)), np.sum(grad_LBA_j_1*ind_speed)])
            else:
                grad_alpha_j_1 = np.sum(grad_LBA_j_1*(ind_acc+ind_neutral+ind_speed))
            
    
                    #Rearranage the gradient of log_pdf of alpha_A
                    
            grad_alpha_j_2 = np.sum(grad_LBA_j_2)
            
            
                      # Rearranage the gradient of log_pdf of alpha_v
                    
            if (model.name[2]== 3): 
                grad_alpha_j_3 = np.hstack([np.sum(grad_LBA_j_3*ind_acc,axis=0), np.sum(grad_LBA_j_3*ind_neutral,axis=0), np.sum(grad_LBA_j_3*ind_speed,axis=0)]).reshape([-1,1])
            elif (model.name[2]== 2): 
                grad_alpha_j_3 = np.hstack([np.sum(grad_LBA_j_3*(ind_acc+ind_neutral),axis=0), np.sum(grad_LBA_j_3*ind_speed,axis=0)]).reshape([-1,1])
            else:
                grad_alpha_j_3 = np.sum(grad_LBA_j_3*(ind_acc+ind_neutral+ind_speed),axis=0).reshape([-1,1])

    
                    # Rearranage the gradient of log_pdf of alpha_s 
                    
          #  grad_alpha_j_4 = np.array([])
            
            
                    # Rearranage the gradient of log_pdf of alpha_tau
                    
            if (model.name[4]== 3): 
                grad_alpha_j_5 = np.vstack([np.sum(grad_LBA_j_5*ind_acc), np.sum(grad_LBA_j_5*ind_neutral), np.sum(grad_LBA_j_5*ind_speed)]) 
            elif (model.name[4]== 2): 
                grad_alpha_j_5 = np.vstack([np.sum(grad_LBA_j_5*(ind_acc+ind_neutral)), np.sum(grad_LBA_j_5*ind_speed)])
            else:
                grad_alpha_j_5 = np.sum(grad_LBA_j_5*(ind_acc+ind_neutral+ind_speed))


    ## store the gradients
           # grad_alpha[:,j] = np.hstack([grad_alpha_j_1.T, grad_alpha_j_2.reshape(-1,1), grad_alpha_j_3.T,grad_alpha_j_5.reshape(-1,1)])
            grad_alpha[:,j] = np.vstack([grad_alpha_j_1.reshape(-1,1)[:], grad_alpha_j_2.reshape(-1,1)[:], grad_alpha_j_3.reshape(-1,1)[:],grad_alpha_j_5.reshape(-1,1)[:]]).T
    
            #time1=time.time()
            #print(f"Runtime of the program is {time1 - start}")
    ## output of the function
    

    # if gradient == True:
      # grad_val = np.vstack([grad_alpha.flatten(order='F').reshape(-1,1), grad_mu, grad_loga])
      #   return ValGrad(np.array(loga), np.array(grad_val)) 
    
    # else:
    #     return ValGrad(np.array(loga), np.array([0])
                       
    grad_val = np.vstack([grad_alpha.flatten(order='F').reshape(-1,1), grad_mu, grad_loga])
    return ValGrad(loga, np.array(grad_val))
                               
                       
# gradient of log p(y|theta) wrt theta

##test
#output=likelihood_Forstmann_Hybrid(model,alpha,data,gradient)
