#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed 26 17:44:11 2021
This is to generate inverse wishart pdf (with Jax compatibility)

np.stats.invwishart([df, scale, seed]) can we use this ?

"""
import jax.numpy as jpn
from numpy.linalg import slogdet #not available in jax
#import jax.numpy as jnp
from multigamma import multigamma
from scipy.stats import invwishart

#import time

def inv_wishart_pdf(X,Psi,v):
    p = Psi.shape[0]
    output = 0.5*v*slogdet(Psi)[1] - 0.5*v*p*jnp.log(2) - jnp.log(multigamma(v/2,p)) -0.5*(v+p+1)*slogdet(X)[1] - 0.5*jnp.trace(Psi/X)
    
    return output
