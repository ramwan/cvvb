# -*- coding: utf-8 -*-
"""
Created on Fri May 21 9:06:52 2021
This function is to generate inverse of gamma pdf 
Same coding style as in original matlab code is used here.
"""
import jax.numpy as jnp
from jax.scipy.special import gammaln
from jax import jit
from functools import partial


def inv_gamma_pdf(x,shape,scale):
    a = shape 
    b = scale
    
    y = a*jnp.log(b) - gammaln(a) - (a+1)*jnp.log(x.reshape(-1,1)) - b/(x.reshape(-1,1))
    return y

#@partial(jit, static_argnums=2)
@jit
def inv_gamma_pdf_jit(a, b, x):
    y = a*jnp.log(b) - gammaln(a) - (a+1)*jnp.log(x.reshape(-1,1)) - b/(x.reshape(-1,1))
    return y
