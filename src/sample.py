import multiprocessing as mp
import sys
import os
import time
import dill

import jax.numpy as jnp
from jax import value_and_grad, local_devices

def run_dill_encoded(payload):
    fun, args = dill.loads(payload)
    return fun(args)

def test_jax(x):
    return jnp.divide(jnp.exp(jnp.power(x, 2)), 10)

def func(numProcesses):
    def f(i):
        time.sleep(1)
        #t1 = time.time_ns()
        #output_grad = value_and_grad(test_jax)
        #_ = output_grad(2.)
        #print("%d: %d, id: %d, pid: %d" % (os.getpid(), i, local_devices()[0].id, local_devices()[0].process_index))
        #t2 = time.time_ns()

        #print("%d: %d, %d us" % (os.getpid(), i, (t2-t1)/1000))

    with mp.Pool(processes=numProcesses) as p:
        res = [p.apply_async(run_dill_encoded, [dill.dumps((f, i))]) for i in range(10)]
        results = [r.get() for r in res]

if __name__ == "__main__":
    numProcesses = 1
    if len(sys.argv) > 1:
        numProcesses = int(sys.argv[1])

    print("Main: %d" % os.getpid())
    func(numProcesses)
