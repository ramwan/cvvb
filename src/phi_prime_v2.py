from jax.scipy.stats import norm
def phi_prime_v2(x):
    return -norm.pdf(x) * x

