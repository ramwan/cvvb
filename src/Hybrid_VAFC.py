"""
This is  Hybrid VB VAFC for Hierarchical LBA model.
@author: Gajan
"""
import numpy as np
from scipy.stats import invwishart

from lib_lba import ValGrad, Model, PriorHyperParams, LearningRateADADELTA, Output,VbSettings, Lambda1
from likelihood_hybrid import likelihood_Hybrid
from prior_density_hybrid import prior_density_hybrid
from p_Sigma_alpha import p_Sigma_alpha
from q_VAFC import q_VAFC
from vech import vech

import multiprocessing as mp
import ctypes
import time
import dill
import os

"""
def run_dill_encoded(payload):
    fun, args = dill.loads(payload)
    return fun(args)
"""

def Hybrid_VAFC(model, data, vb_settings, numProcessors):
    lambda1 = vb_settings.initial
    I = vb_settings.I
    r = vb_settings.r
    max_iter = vb_settings.max_iter
    max_norm = vb_settings.max_norm
    patience_parameter = vb_settings.patience_parameter
    window = vb_settings.window
    silent= vb_settings.silent
    prior_par = model.prior_par
    learning_rate = vb_settings.learning_rate

    data_RE = data['RE']
    data_RT = data['RT']
    data_E = data['E']

    LB = np.zeros([max_iter, 1])

    J = len(data_RT)
    D_alpha = int(np.sum(model.index))
    p = D_alpha * (J + 2)
    v_a = prior_par.v_a  # check
    df = v_a + D_alpha + J - 1  # check this again(what is this?)
    df = np.double(df)  # make sure df is a scalar

    skip_count = 0  # count for gradients skips due to inf/nan

    # choose the optimizer
    if isinstance(learning_rate, LearningRateADADELTA):
        v = learning_rate.v
        eps = learning_rate.eps  # hyperparameters
        E_g2 = np.zeros([p * (r + 2), 1])
        E_delta2 = np.zeros([p * (r + 2), 1])

    t = 0
    t_smooth = 0 # iteration index for LB_smooth
    LB_smooth = np.zeros([max_iter - window + 1, 1])
    patience = 0

    lambda_best = lambda1
    max_best = LB_smooth[0]

    stopping_rule = False
    converge = 'reach_max_iter'

    """
    def inner_loop(runNum):
        t1 = time.time_ns()
        LB = np.zeros([max_iter, 1])

        J = len(data_RT)
        D_alpha = int(np.sum(model.index))
        p = D_alpha * (J + 2)
        v_a = prior_par.v_a  # check
        df = v_a + D_alpha + J - 1  # check this again(what is this?)
        df = np.double(df)  # make sure df is a scalar

        # (1) Generate theta
        z = np.random.multivariate_normal(np.zeros([1, r]).flatten(), np.eye(r), 1).T
        epsilon = np.random.multivariate_normal(np.zeros([1, p]).flatten(), np.eye(p), 1).T

        # theta_1 = (alpha_1,...,alpha_J,mu_alpha,log a_1,...,log a_D)
        theta_1 = (mu.reshape(-1,1) + B @ z + d * epsilon).squeeze()
        alpha = np.reshape(theta_1[:D_alpha * J], [D_alpha, J], order='F')
        mu_alpha = theta_1[D_alpha * J:D_alpha * (J + 1)]
        log_a = theta_1[D_alpha * (J + 1):]
        a = np.exp(log_a)

        Psi = 2 * v_a * np.diag(1 / a)
        for j in range(0, J):
            Psi = Psi + (alpha[:, j] - mu_alpha).reshape(-1, 1) @ (alpha[:, j] - mu_alpha).reshape(-1, 1).T
        Sigma_alpha = invwishart.rvs(df, Psi)

        # (2) Calculate the likelihood, prior and q_vb
        t3 = time.time_ns()
        like = likelihood_Hybrid(model, alpha, data, True)
        t4 = time.time_ns()
        #print("Run %d likelihood time (%d): %d ms" % (runNum, os.getpid(), (t4-t3)/1000000))

        prior = prior_density_hybrid(model, alpha, mu_alpha, log_a, Sigma_alpha, prior_par)
        t5 = time.time_ns()
        #print("Run %d prior time (%d): %d ms" % (runNum, os.getpid(), (t5-t4)/1000000))
        q_lambda = q_VAFC(theta_1, mu, B, d)  #
        t6 = time.time_ns()
        #print("Run %d qvafc time (%d): %d ms" % (runNum, os.getpid(), (t6-t5)/1000000))
        q_Sigma = p_Sigma_alpha(Sigma_alpha, df, v_a, alpha, mu_alpha, log_a)  #
        t7 = time.time_ns()
        #print("Run %d psigmaalpha time (%d): %d ms" % (runNum, os.getpid(), (t7-t6)/1000000))
        #print("Run %d calculation time (%d): %d ms" % (runNum, os.getpid(), (t7-t3)/1000000))

        # (3) Estimate the lower bound
        LBs = like.val + prior.val - q_lambda.val - q_Sigma.val
        # (4) Compute the gradients
        grad_theta_1_LB = like.grad + prior.grad - q_lambda.grad - q_Sigma.grad

        temp = grad_theta_1_LB
        gradmu = temp.flatten()
        gradB = temp @ z.T
        gradd = (temp * epsilon).flatten()
        t2 = time.time_ns()
        #print("Run %d time (%d): %d ms" % (runNum, os.getpid(), (t2-t1)/1000000))

        return (runNum, LBs, gradmu, gradB, gradd)
    """

    #with mp.Pool(processes=numProcessors) as pool:
    while t < max_iter and not stopping_rule:
        mu = lambda1.mu.squeeze()
        B = lambda1.B
        d = lambda1.d

        # intialize grad arrays
        gradmu = np.zeros([p, I])
        gradB = np.zeros([p, r, I])
        gradd = np.zeros([p, I])

        LBs = np.zeros([I, 1])
        for i in range(0, I):
            """
        t1 = time.time_ns()
        runs = [pool.apply_async(run_dill_encoded, [dill.dumps((inner_loop, i))] )\
                for i in range(I)]
        results = [run.get() for run in runs]
        t2 = time.time_ns()
        print("Run time (%d): %d ms" % (os.getpid(), (t2-t1)/1000000))
        a = b
        for res in results:
            ind = res[0]
            LBs[ind] = res[1]
            gradmu[:, ind] = res[2]
            gradB[:, :, ind] = res[3]
            gradd[:, ind] = res[4]
            """
            # (1) Generate theta
            z = np.random.multivariate_normal(np.zeros([1, r]).flatten(), np.eye(r), 1).T
            epsilon = np.random.multivariate_normal(np.zeros([1, p]).flatten(), np.eye(p), 1).T

            # theta_1 = (alpha_1,...,alpha_J,mu_alpha,log a_1,...,log a_D)
            theta_1 = (mu.reshape(-1,1) + B @ z + d * epsilon).squeeze()
            alpha = np.reshape(theta_1[:D_alpha * J], [D_alpha, J], order='F')
            mu_alpha = theta_1[D_alpha * J:D_alpha * (J + 1)]
            log_a = theta_1[D_alpha * (J + 1):]
            a = np.exp(log_a)

            Psi = 2 * v_a * np.diag(1 / a)
            for j in range(0, J):
                Psi = Psi + (alpha[:, j] - mu_alpha).reshape(-1, 1) @ (alpha[:, j] - mu_alpha).reshape(-1, 1).T
            Sigma_alpha = invwishart.rvs(df, Psi)

            # (2) Calculate the likelihood, prior and q_vb
            like = likelihood_Hybrid(model, alpha, data, True)

            prior = prior_density_hybrid(model, alpha, mu_alpha, log_a, Sigma_alpha, prior_par)
            q_lambda = q_VAFC(theta_1, mu, B, d)  #
            q_Sigma = p_Sigma_alpha(Sigma_alpha, df, v_a, alpha, mu_alpha, log_a)  #

            # (3) Estimate the lower bound
            LBs[i] = like.val + prior.val - q_lambda.val - q_Sigma.val
            # (4) Compute the gradients
            grad_theta_1_LB = like.grad + prior.grad - q_lambda.grad - q_Sigma.grad

            temp = grad_theta_1_LB
            gradmu[:, i] = temp.flatten()
            gradB[:, :, i] = temp @ z.T
            gradd[:, i] = (temp * epsilon).flatten()

        # Estimate the Lower Bound
        LB[t] = np.mean(LBs)

        if (silent == False) & (np.mod(t + 1, 100) == 0):
            print(['iteration ', str(t), ' || LB: ', str(LB[t]), ' || standard error: ', str(np.std(LBs))])

        # Estimate the gradients
        grad_mu = np.mean(gradmu, 1)
        grad_B = np.mean(gradB, 2)
        grad_D = np.mean(gradd, 1)

        g = np.vstack([grad_mu.reshape(-1, 1), grad_B.flatten(order='F').reshape(-1, 1),
                       grad_D.reshape(-1, 1)])  # Stack gradient of LB into 1 column

        # check for nan/inf value in inf
        if (np.sum(np.isnan(g) + np.isinf(g)) > 0):
            if t == 0:
                LB[t] = np.mean(LBs[np.isfinite(LBs)])  # get LBs mean for finite values
            else:
                LB[t] = LB[t - 1]  # assign previous value, t>0

            lambda1 = lambda1  # optional line
            skip_count = skip_count + 1

        else:
            # Gradient clipping
            norm_g = np.linalg.norm(g)
            if norm_g > max_norm:
                g = max_norm * g / norm_g

            # Update learning rate
            if isinstance(learning_rate, LearningRateADADELTA):
                E_g2 = v * E_g2 + (1 - v) * (g ** 2);
                rho = np.sqrt(E_delta2 + eps) / np.sqrt(E_g2 + eps)
                Delta = rho * g;
                E_delta2 = v * E_delta2 + (1 - v) * (Delta ** 2)

            # Update Lambda
            vec_lambda = np.vstack(
                [lambda1.mu.reshape(-1, 1), lambda1.B.flatten(order='F').reshape(-1, 1), lambda1.d.reshape(-1, 1)])
            vec_lambda = vec_lambda + Delta
            lambda1.mu = vec_lambda[:p]
            lambda1.B = np.reshape(vec_lambda[p:(p * (r + 1))], (p, r), order='F')
            lambda1.B = np.tril(lambda1.B)  # IMPORTANT: Upper Triangle of B must be zero !!!
            lambda1.d = vec_lambda[(p * (r + 1)):]

        # Stopping Rule:
        if t >= window:
            LB_smooth[t_smooth] = np.mean(LB[t + 1 - window:t + 1])

            if (LB_smooth[t_smooth] <= max_best) | (np.abs(LB_smooth[t_smooth] - LB_smooth[t_smooth - 1]) < 0.00001):
                patience = patience + 1
            else:
                patience = 0
                lambda_best = lambda1
                max_best = LB_smooth[t_smooth]

            if (patience > patience_parameter):
                stopping_rule = True
                if(np.std(LB_smooth[t_smooth - patience_parameter+1:t_smooth]) > 10):
                    print( ['  Warning: VB might not converge to a good local mode'
                            + 'Initial LB = ' + str(np.round(LB[0],1)) + ' || max LB = ' + str( np.round(max_best,1))])
                    converge = True
                else:
                    converge = True
            t_smooth += 1
        t += 1

    ## SAVE THE OUTPUT
    if vb_settings.generate_samples == True:
        lambda1 = lambda_best
        mu = lambda1.mu
        B = lambda1.B
        d = lambda1.d

        v_a = prior_par.v_a
        df = v_a + D_alpha + J - 1

        N = 10000 #posterior samples
        theta_VB = np.zeros([int(p+D_alpha*(D_alpha+1)/2),N])

        for i in range (0,N):
            epsilon = np.random.randn(p,1)
            z = np.random.randn(r,1)

            theta_1 = (mu.reshape(-1,1) + B @z + d.reshape(-1,1)*epsilon).squeeze() # theta_1 = (alpha_1,...,alpha_J,mu_alpha,log a_1,...,log a_D)
            alpha = np.reshape(theta_1[:D_alpha*J],[D_alpha,J],order='F')
            mu_alpha = theta_1[D_alpha*J :D_alpha*(J+1)]
            log_a = theta_1[D_alpha*(J+1):]
            a = np.exp(log_a)

            Psi = 2*v_a*np.diag(1/a)
            for j in range (0,J):
                Psi = Psi + (alpha[:,j]-mu_alpha).reshape(-1,1) @ (alpha[:,j]-mu_alpha).reshape(-1,1).T

            Sigma_alpha = invwishart.rvs(df,Psi)

            C = np.linalg.cholesky(Sigma_alpha)
            C_star = np.copy(C)
            np.fill_diagonal(C_star, np.log(np.diag(C)))

            theta_VB[:,i] = np.hstack([alpha.flatten(order='F'), mu_alpha, vech(C_star).squeeze(), log_a])

        return Output(lambda_best, LB[:t - 1], LB_smooth[:t_smooth - 1], max_best, skip_count, converge,theta_VB)

    #output as a object
    else:
        return Output(lambda_best, LB[:t - 1], LB_smooth[:t_smooth - 1], max_best, skip_count, converge,np.array([0]))
