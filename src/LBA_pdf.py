import numpy as np

from CDF_c_v2 import cdf_c
from pdf_c_v2 import pdf_c

def obtain_log_pdf_LBA(c,t,b,A,v,s,tau,gradient,sizes):
    c = np.array(c)
    t = np.array(t)

    ind_acc2 = c == 2  # not true
    ind_acc1 = c == 1  # true

    """
    b_c = ind_acc2 * b[:, 1].reshape(-1, 1) + ind_acc1 * b[:, 0].reshape(-1, 1)
    A_c = ind_acc2 * A[:, 1].reshape(-1, 1) + ind_acc1 * A[:, 0].reshape(-1, 1)
    v_c = ind_acc2 * v[:, 1].reshape(-1, 1) + ind_acc1 * v[:, 0].reshape(-1, 1)
    s_c = ind_acc2 * s[:, 1].reshape(-1, 1) + ind_acc1 * s[:, 0].reshape(-1, 1)
    tau_c = ind_acc2 * tau[:, 1].reshape(-1, 1) + ind_acc1 * tau[:, 0].reshape(-1, 1)

    b_k = ind_acc2 * b[:, 0].reshape(-1, 1) + ind_acc1 * b[:, 1].reshape(-1, 1)
    A_k = ind_acc2 * A[:, 0].reshape(-1, 1) + ind_acc1 * A[:, 1].reshape(-1, 1)
    v_k = ind_acc2 * v[:, 0].reshape(-1, 1) + ind_acc1 * v[:, 1].reshape(-1, 1)
    s_k = ind_acc2 * s[:, 0].reshape(-1, 1) + ind_acc1 * s[:, 1].reshape(-1, 1)
    tau_k = ind_acc2 * tau[:, 0].reshape(-1, 1) + ind_acc1 * tau[:, 1].reshape(-1, 1)
    """

    b_c = ind_acc2 * b[:, 1] + ind_acc1 * b[:, 0]
    A_c = ind_acc2 * A[:, 1] + ind_acc1 * A[:, 0]
    v_c = ind_acc2 * v[:, 1] + ind_acc1 * v[:, 0]
    s_c = ind_acc2 * s[:, 1] + ind_acc1 * s[:, 0]
    tau_c = ind_acc2 * tau[:, 1] + ind_acc1 * tau[:, 0]

    b_k = ind_acc2 * b[:, 0] + ind_acc1 * b[:, 1]
    A_k = ind_acc2 * A[:, 0] + ind_acc1 * A[:, 1]
    v_k = ind_acc2 * v[:, 0] + ind_acc1 * v[:, 1]
    s_k = ind_acc2 * s[:, 0] + ind_acc1 * s[:, 1]
    tau_k = ind_acc2 * tau[:, 0] + ind_acc1 * tau[:, 1]

    # gradients will have a return shape of [sum(sizes) x 1, sum(sizes) x 5]
    # if we don't care about gradients, just ignore second entry
    f_c = pdf_c(t, b_c, A_c, v_c, s_c, tau_c)
    F_k = cdf_c(t, b_k, A_k, v_k, s_k, tau_k)

    # now we will have J entries in log_LBA
    log_LBA = []
    pre = 0
    for size in sizes:
        log_LBA.append(np.sum(f_c[0][pre:pre+size]) + np.sum(F_k[0][pre:pre+size]))
        pre += size

    if gradient == True:
        """
        grad_b_acc1 = f_c[1][:, 0].reshape(-1, 1) * ind_acc1 + F_k[1][:, 0].reshape(-1, 1) * ind_acc2
        grad_A_acc1 = f_c[1][:, 1].reshape(-1, 1) * ind_acc1 + F_k[1][:, 1].reshape(-1, 1) * ind_acc2
        grad_v_acc1 = f_c[1][:, 2].reshape(-1, 1) * ind_acc1 + F_k[1][:, 2].reshape(-1, 1) * ind_acc2
        grad_s_acc1 = f_c[1][:, 3].reshape(-1, 1) * ind_acc1 + F_k[1][:, 3].reshape(-1, 1) * ind_acc2
        grad_tau_acc1 = f_c[1][:, 4].reshape(-1, 1) * ind_acc1 + F_k[1][:, 4].reshape(-1, 1) * ind_acc2

        grad_b_acc2 = f_c[1][:, 0].reshape(-1, 1) * ind_acc2 + F_k[1][:, 0].reshape(-1, 1) * ind_acc1
        grad_A_acc2 = f_c[1][:, 1].reshape(-1, 1) * ind_acc2 + F_k[1][:, 1].reshape(-1, 1) * ind_acc1
        grad_v_acc2 = f_c[1][:, 2].reshape(-1, 1) * ind_acc2 + F_k[1][:, 2].reshape(-1, 1) * ind_acc1
        grad_s_acc2 = f_c[1][:, 3].reshape(-1, 1) * ind_acc2 + F_k[1][:, 3].reshape(-1, 1) * ind_acc1
        grad_tau_acc2 = f_c[1][:, 4].reshape(-1, 1) * ind_acc2 + F_k[1][:, 4].reshape(-1, 1) * ind_acc1
        """
        grad_b_acc1 = f_c[1][0, :] * ind_acc1 + F_k[1][0, :]
        grad_A_acc1 = f_c[1][1, :] * ind_acc1 + F_k[1][1, :]
        grad_v_acc1 = f_c[1][2, :] * ind_acc1 + F_k[1][2, :]
        grad_s_acc1 = f_c[1][3, :] * ind_acc1 + F_k[1][3, :]
        grad_tau_acc1 = f_c[1][4, :] * ind_acc1 + F_k[1][4, :]

        grad_b_acc2 = f_c[1][0, :] * ind_acc2 + F_k[1][0, :]
        grad_A_acc2 = f_c[1][1, :] * ind_acc2 + F_k[1][1, :]
        grad_v_acc2 = f_c[1][2, :] * ind_acc2 + F_k[1][2, :]
        grad_s_acc2 = f_c[1][3, :] * ind_acc2 + F_k[1][3, :]
        grad_tau_acc2 = f_c[1][4, :] * ind_acc2 + F_k[1][4, :] * ind_acc1

        # should be of size [sum(sizes) x (2*5)]
        # output_grad should be similar to log_LBA with size [J x whatever]
        output_grad = []
        pre = 0
        for size in sizes:
            output_grad.append(np.vstack((\
                grad_b_acc1[pre:pre+size], grad_b_acc2[pre:pre+size],\
                grad_A_acc1[pre:pre+size], grad_A_acc2[pre:pre+size],\
                grad_v_acc1[pre:pre+size], grad_v_acc2[pre:pre+size],\
                grad_s_acc1[pre:pre+size], grad_s_acc2[pre:pre+size],\
                grad_tau_acc1[pre:pre+size], grad_tau_acc2[pre:pre+size]\
            )).T)
            pre += size

        return (np.array(log_LBA), output_grad)
    else:
        # log_element of shape (J, size[j])
        log_element = []
        pre = 0
        for size in sizes:
            log_element.append(f_c[0][pre:pre + size] + F_k[0][pre:pre + size])
            pre += size

        return (np.array(log_LBA), np.array([0]),log_element)  # additionally sent log element wise



def LBA_pdf(c,t,b,A,v,s,tau,gradient,sizes):
    output_val_grad = obtain_log_pdf_LBA(c, t, b, A, v, s, tau, gradient, sizes)
    temp = list(output_val_grad)

    if gradient == True:
        output_val_grad = (np.array(temp[0], dtype=object), np.array(temp[1], dtype=object))
        return output_val_grad
    else:
        output_val_grad = (np.array(temp[0], dtype=object),\
                           np.array(temp[1], dtype=object),\
                           np.array(temp[2], dtype=object))
        return output_val_grad
