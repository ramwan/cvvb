import os
import sys
import time

from model_specification import model_specification

help_option = ["-h", "--help", "help", "h"]
DEBUG = False

max_iter = 100
max_norm = 1000
K = 5
r = 15
I = 10
window = 10
patience_parameter = 20
a_t = 0.9

if __name__ == "__main__":
    numProcessors = 1
    if len(sys.argv) > 1:
        if sys.argv[1] in help_option:
            print("Usage:    python3 main.py [numProcs=(int)] [debug=(bool)]")
            print("For help: python3 main.py -h")
            sys.exit(0)

        for arg in sys.argv[1:]:
            a = arg.split('=')
            if a[0].strip() == "numProcs":
                try:
                    numProcessors = int(a[1])
                except:
                    print("Invalid number of processors given to program, please specify integer.")
                    sys.exit(1)
            elif a[0].strip() == "debug":
                try:
                    DEBUG = bool(a[1])
                except:
                    print("Invalid debug value, please specify boolean.")
                    sys.exit(1)
            else:
                print("Unknown option: ", a[0])
                sys.exit(1)
                

    dataset_path = "./Forstmann.mat"
    initial_path_models = "./initial_values_forstmann.mat"
    dmat = sio.loadmat(initial_path_models)

    (M, models, data) = model_specification("Forstmann", dataset_path,\
                                            max_iter=max_iter, max_norm=max_norm,\
                                            K=K, r=r, I=I, window=window,\
                                            patience_param=patience_parameter,\
                                            annealing=a_t)

    results = []
    for m in range(M):
        result = m.run_model(""" args """)
        results.append(result)

    f = open(time.asctime(time.localtime()), 'wb')
    pickle.dump(results, f)
    f.close()
