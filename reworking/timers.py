import time

global_timers = {}

class function_timer():
    def __init__(self, name):
        self.name = name
        self.__end = -1
        # start timer immediately, units of us
        self.__start = time.time_ns() / 1000

    def stop(self):
        self.__end = time.time_ns() / 1000
        
        if self.name not in global_timers:
            global_timers[self.name] = []

        global_timers[self.name].append(self.__end - self.__start)


def timer_decorator(func):
    def wrapper(*args, **kwargs):
        t = function_timer(func.__name__)
        ret = func(*args, **kwargs)
        t.stop()
        return ret
    return wrapper

def timers_average_runtimes():
    for k in global_timers:
        sum = 0
        for t in global_timers[k]:
            # python would rather throw a memory exception than overflow
            sum += t
        sum /= len(global_timers[k])
        print(k, " (average time, us): ", sum, \
              "|| longest runtime (us) : ", max(global_timers[k]))
