import numpy as np
import scipy.io as sio

class PriorHyperParams:
    def __init__(self, mu, cov, v_a, A_d):
        self.mu = mu
        self.cov = cov
        self.v_a = v_a
        self.A_d = A_d

class Model:
    def __init__(self, name, index, num_subjects, dim, prior_params):
        self.name = name
        self.index = index
        self.num_subjects = num_subjects
        self.dim = dim
        self.prior_par = prior_params

    def name_string(self):
       return ' c ~'+str(self.name[0]) + '| A ~ '+str(self.name[1]) + '| v ~ ' +\
              str(self.name[2]) + '| s ~ ' + str(self.name[3]) + '| t0 ~ ' +\
              str(self.name[4])

    def random_effects(self):
        return np.sum(self.index)

    def run_model(self):
        pass

@timer_decorator
def model_specification(dataset, datapath, max_iter=100, max_norm=1000,\
                        K=5, r=15, I=10, window=10, patience_param=20,\
                        annealing=0.9):
    if dataset == "Forstmann":
        # total number of models
        M = 27 
        # each value corresponds to z_c, z_v, z_tau
        model_indices = np.array([[1, 1, 1],
                                  [1, 1, 2],
                                  [1, 1, 3],
                                  [1, 2, 3],
                                  [1, 2, 2],
                                  [1, 2, 1],
                                  [1, 3, 1],
                                  [1, 3, 2],
                                  [1, 3, 3],
                                  [2, 3, 3],
                                  [2, 3, 2],
                                  [2, 3, 1],
                                  [2, 2, 1], 
                                  [2, 2, 2], 
                                  [2, 2, 3], 
                                  [2, 1, 3], 
                                  [2, 1, 2], 
                                  [2, 1, 1], 
                                  [3, 1, 1], 
                                  [3, 1, 2], 
                                  [3, 1, 3], 
                                  [3, 2, 3], 
                                  [3, 2, 2], 
                                  [3, 2, 1], 
                                  [3, 3, 1], 
                                  [3, 3, 2], 
                                  [3, 3, 3]])

        mat = sio.loadmat(datapath)
        mdata = mat['data']
        mdtype = mdata.dtype

        data = {}
        for n in mdtype.names:
            data[n] = np.array([np.squeeze(mdata[n][0, 0][m, 0])\
                                    for m in range(len(mdata[n][0, 0])) ],
                                dtype=object)

        constraints = np.array([1, 2, 3])
        models = []
        for i in range(M):
            ind = model_indices[i, :]
            model_name = np.array([constraints[ind[0] - 1], 1,\
                                   constraints[ind[1] - 1], 0,\
                                   constraints[ind[2] - 1]])

            model_index = np.zeros([5,1], dtype=int)
            model_index[0] = ind[0]
            model_index[1] = 1
            model_index[2] = ind[1] * 2
            model_index[4] = ind[2]

            D_latent = np.int(np.sum(model_index))
            prior_par_mu = np.zeros([D_latent, 1])
            prior_par_cov = np.eye(D_latent)
            prior_par_v_a = 2
            prior_par_A_d = np.ones([D_latent, 1])

            prior_par = PriorHyperParams(prior_par_mu, prior_par_cov,\
                                         prior_par_v_a, prior_par_A_d)

            J = 19
            p = D_latent * (J + 2)
            models.append(Model(model_name, model_index.squeeze(), J, p, prior_par))

        return (M, models, data)
    else:
        print("invalid dataset: ", dataset)
        return (-1, None, None)
