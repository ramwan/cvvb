import jax.numpy as jnp
import jax.numpy.linalg as jnl
from jax import value_and_grad,jit

from helpers import ValGrad, log_inv_gamma_pdf, multigamma
from timers import timer_decorator


@timer_decorator
#@jit
def obtain_log_prior_VAFC_LBA(D_alpha, prior_par_v_a, prior_par_A_d,\
                              J, alpha, mu_alpha, log_a, Sigma_alpha):
    J = jnp.array(J)
    v = prior_par_v_a + D_alpha - 1
    
    sa = Sigma_alpha # abbreviation for long equation ahead
    sa_inv = jnp.linalg.inv(sa)
    A = lambda sa1, alpha1, mu1:\
        sa_inv @ (alpha1 - (jnp.array([mu1.flatten()] * alpha1.shape[1]).T))

    log_prior = lambda alpha1, mu1, loga1:\
        - 0.5*D_alpha*(J+1)*jnp.log(2*jnp.pi)\
        - 0.5*(J + v + D_alpha +1)*jnp.linalg.slogdet(sa)[1]\
        - 0.5*jnp.trace((alpha1-(jnp.array([mu1.flatten() ]*alpha1.shape[1]).T)).T \
            @ A(sa, alpha1, mu1))\
        - 0.5*(mu1.T @ mu1) \
        + 0.5* v * (D_alpha* jnp.log(2*prior_par_v_a) - jnp.sum(loga1))\
        - 0.5*v*D_alpha*jnp.log(2) \
        - jnp.log(multigamma(v/2, D_alpha))\
        - prior_par_v_a * jnp.sum(jnp.diag(sa_inv)/jnp.exp(loga1)) \
        + jnp.sum(log_inv_gamma_pdf(jnp.exp(loga1), 0.5*jnp.ones([D_alpha]), 1/(prior_par_A_d**2)))\
        + jnp.sum(loga1)

    output_grad = value_and_grad(log_prior, (0, 1, 2))
    (val, [grad_alpha, grad_mu, grad_loga]) = output_grad(alpha, mu_alpha, log_a)

    grad_alpha = jnp.stack(grad_alpha, axis=1).flatten()
    prior_grad = jnp.vstack((grad_alpha.reshape(-1,1),grad_mu.reshape(-1,1),grad_loga.reshape(-1,1)))

    return (val,prior_grad)
 
def prior_density_hybrid(model, alpha, mu_alpha, a, Sigma_alpha):
    D_alpha = model.random_effects()
    J = model.num_subjects
    log_a = jnp.log(a)

    v_a = jnp.array(model.prior_par.v_a)
    A_d = model.prior_par.A_d

    theta_grad = obtain_log_prior_VAFC_LBA(D_alpha, v_a, A_d, J, alpha,\
                                           mu_alpha, log_a, Sigma_alpha)
    return ValGrad(theta_grad[0], theta_grad[1])
