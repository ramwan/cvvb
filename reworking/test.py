import unittest

import numpy as np

from helpers import log_inv_gamma_pdf, multigamma
from LBA import cdf_c, pdf_c, w1, w2, x1, z1, log_pdf_LBA
from prior_density_hybrid import obtain_log_prior_VAFC_LBA

diff_bound = 1e-4

class TestLBA(unittest.TestCase):
    def test_w1(self):
        self.assertTrue(np.abs(w1(2,2,2,2,0.5) - (-0.3333)) < 1e-4)

    def test_w2(self):
        self.assertTrue(np.abs(w2(2,2,2,0.5) - 0.6667) < 1e-4)

    def test_x1(self):
        self.assertTrue(np.abs(x1(2,2,2,2,2,0.5) - 0.2420) < 1e-4)

    def test_z1(self):
        self.assertTrue(np.abs(z1(2,2,2,2,0.5) - 0.3774) < 1e-4)

    def cdf_diff(self, val):
        return np.abs(val) < diff_bound

    def test_cdf_1(self):
        t = np.array([1.])
        b = np.array([1.2])
        A = np.array([0.6])
        v = np.array([1.])
        s = np.array([1.])
        tau = np.array([0.1])
        val, grads = cdf_c(t, b, A, v, s, tau)
        
        self.assertTrue( self.cdf_diff(-0.6931 - val[0]) )      # val
        self.assertTrue( self.cdf_diff(0.8704 - grads[0]) )  # grad b
        self.assertTrue( self.cdf_diff(-0.4352 - grads[1]) ) # grad A
        self.assertTrue( self.cdf_diff(-0.7834 - grads[2]) )  # grad v
        self.assertTrue( self.cdf_diff(7.6e-17 - grads[3]) )  # grad s
        self.assertTrue( self.cdf_diff(0.8704 - grads[4]) )  # grad tau

    def test_cdf_2(self):
        t = np.array([2.])
        b = np.array([2.])
        A = np.array([2.])
        v = np.array([2.])
        s = np.array([2.])
        tau = np.array([0.5])
        val, grads = cdf_c(t, b, A, v, s, tau)
        
        self.assertTrue( self.cdf_diff(-1.3611 - val[0]) )      # val
        self.assertTrue( self.cdf_diff(0.4111 - grads[0]) )  # grad b
        self.assertTrue( self.cdf_diff(-0.1906 - grads[1]) ) # grad A
        self.assertTrue( self.cdf_diff(-0.6166 - grads[2]) ) # grad v
        self.assertTrue( self.cdf_diff(0.3961 - grads[3]) )  # grad s
        self.assertTrue( self.cdf_diff(0.2940 - grads[4]) )  # grad tau

    def test_cdf_3(self):
        t = np.array([1., 2.])
        b = np.array([1., 2.])
        A = np.array([1., 2.])
        v = np.array([1., 2.])
        s = np.array([1., 2.])
        tau = np.array([0.1, 0.5])
        val, grads = cdf_c(t, b, A, v, s, tau)

        self.assertTrue( self.cdf_diff(-1.0898 - val[0]) )
        self.assertTrue( self.cdf_diff(-1.3611 - val[1]) )
        self.assertTrue( self.cdf_diff(1.1466 - grads[0][0]) )
        self.assertTrue( self.cdf_diff(0.4111 - grads[0][1]) )
        self.assertTrue( self.cdf_diff(-0.5282 - grads[1][0]) )
        self.assertTrue( self.cdf_diff(-0.1906 - grads[1][1]) )
        self.assertTrue( self.cdf_diff(-1.032 - grads[2][0]) )
        self.assertTrue( self.cdf_diff(-0.6166 - grads[2][1]) )
        self.assertTrue( self.cdf_diff(0.4135 - grads[3][0]) )
        self.assertTrue( self.cdf_diff(0.3961 - grads[3][1]) )
        self.assertTrue( self.cdf_diff(0.6871 - grads[4][0]) )
        self.assertTrue( self.cdf_diff(0.2940 - grads[4][1]) )

    def pdf_diff(self, val):
        return np.abs(val) < diff_bound

    def test_pdf_1(self):
        t = np.array([1.])
        b = np.array([1.])
        A = np.array([1.])
        v = np.array([1.])
        s = np.array([1.])
        tau = np.array([0.1])
        val, grads = pdf_c(t, b, A, v, s, tau)
        
        self.assertTrue( self.pdf_diff(-1.4651 - val[0]) )      # val
        self.assertTrue( self.pdf_diff(2.1184 - grads[0]) )  # grad b
        self.assertTrue( self.pdf_diff(-1 - grads[1]) ) # grad A
        self.assertTrue( self.pdf_diff(-0.2379 - grads[2]) )  # grad v
        self.assertTrue( self.pdf_diff(-0.8806 - grads[3]) )  # grad s
        self.assertTrue( self.pdf_diff(2.3538 - grads[4]) )  # grad tau

    def test_pdf_2(self):
        t = np.array([2.])
        b = np.array([2.])
        A = np.array([2.])
        v = np.array([2.])
        s = np.array([2.])
        tau = np.array([0.5])
        val, grads = pdf_c(t, b, A, v, s, tau)
        
        self.assertTrue( self.pdf_diff(-2.5853 - val[0]) )      # val
        self.assertTrue( self.pdf_diff(1.1126 - grads[0]) )  # grad b
        self.assertTrue( self.pdf_diff(-0.5 - grads[1]) ) # grad A
        self.assertTrue( self.pdf_diff(-0.2707 - grads[2]) ) # grad v
        self.assertTrue( self.pdf_diff(-0.3420 - grads[3]) )  # grad s
        self.assertTrue( self.pdf_diff(1.4835 - grads[4]) )  # grad tau

    def test_pdf_3(self):
        t = np.array([1., 2.])
        b = np.array([1., 2.])
        A = np.array([1., 2.])
        v = np.array([1., 2.])
        s = np.array([1., 2.])
        tau = np.array([0.1, 0.5])
        val, grads = pdf_c(t, b, A, v, s, tau)

        self.assertTrue( self.pdf_diff(-1.4651 - val[0]) )
        self.assertTrue( self.pdf_diff(-2.5853 - val[1]) )
        self.assertTrue( self.pdf_diff(2.1184 - grads[0][0]) )
        self.assertTrue( self.pdf_diff(1.1126 - grads[0][1]) )
        self.assertTrue( self.pdf_diff(-1 - grads[1][0]) )
        self.assertTrue( self.pdf_diff(-0.5 - grads[1][1]) )
        self.assertTrue( self.pdf_diff(-0.2379 - grads[2][0]) )
        self.assertTrue( self.pdf_diff(-0.2707 - grads[2][1]) )
        self.assertTrue( self.pdf_diff(-0.8806 - grads[3][0]) )
        self.assertTrue( self.pdf_diff(-0.3420 - grads[3][1]) )
        self.assertTrue( self.pdf_diff(2.3538 - grads[4][0]) )
        self.assertTrue( self.pdf_diff(1.4835 - grads[4][1]) )

    def lba_diff(self, val):
        return np.abs(val) < diff_bound

    def test_LBA_1(self):
        t = np.array([[1., 1.]])
        b = np.array([[1., 1.]])
        A = np.array([[1., 1.]])
        v = np.array([[1., 1.]])
        s = np.array([[1., 1.]])
        tau = np.array([[0.1, 0.1]])
        c = np.array([1])
        t = np.array([1])

        val, grads = log_pdf_LBA(c,t,b,A,v,s,tau,True,[1])

        self.assertTrue( -2.5549 - val[0] )

        # our grads will come out as shape
        # (num participants,
        #  trials per participants,
        #  5 random variables * 2 accumulators = 10)
        #
        # in this test, we have 1 participant and 1 trial for shape (1, 1, 10)
        # with values stacked as indicated
        self.assertTrue( self.lba_diff(2.1184 - grads[0][0][0]) ) # grad b accum 1
        self.assertTrue( self.lba_diff(1.1466 - grads[0][0][1]) ) # grad b accum 2
        self.assertTrue( self.lba_diff(-1 - grads[0][0][2]) ) # grad A 
        self.assertTrue( self.lba_diff(-0.5282 - grads[0][0][3]) )
        self.assertTrue( self.lba_diff(-0.2379 - grads[0][0][4]) ) # grad v
        self.assertTrue( self.lba_diff(-1.0320 - grads[0][0][5]) )
        self.assertTrue( self.lba_diff(-0.8806 - grads[0][0][6]) ) # grad s
        self.assertTrue( self.lba_diff(0.4135 - grads[0][0][7]) )
        self.assertTrue( self.lba_diff(2.3538 - grads[0][0][8]) ) # grad tau
        self.assertTrue( self.lba_diff(0.6871 - grads[0][0][9]) )

class TestPriorDensity(unittest.TestCase):
    def diff(self, val):
        return np.abs(val) < diff_bound

    def test_multigamma(self):
        self.assertTrue( self.diff(np.log(multigamma(1,1)) - 0.0) )
        self.assertTrue( self.diff(np.log(multigamma(1,2)) - 1.1447) )
        self.assertTrue( self.diff(np.log(multigamma(3,6)) - 10.0149) )

    def test_log_inv_gamma_pdf(self):
        a = np.array([1,1])
        b = np.array([1,2])
        c = np.array([1,3])

        self.assertTrue(\
            (self.diff(log_inv_gamma_pdf(a, b, c)) - [-1., -1.3069]).all())

    def test_log_prior(self):
        D = 1
        v_a = 1
        A_d = np.array([[1.]])
        J = 1
        alpha = np.array([[1.]])
        mu_alpha = np.array([1.])
        a = np.array([1.]) # we have a discrepency in the grad of log_a
        log_a = np.log(a)
        Sigma = np.array([[1.]])

        v, g = obtain_log_prior_VAFC_LBA(D, v_a, A_d, J, alpha, mu_alpha, log_a, Sigma)
        print("val: ", v)
        print("grads: ", g)
        
        self.assertTrue( self.diff(v - -5.4826) )

if __name__ == "__main__":
    unittest.main()
