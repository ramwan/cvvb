import jax.numpy as jnp
import jax.lax as lax
from jax.scipy.special import gammaln
from jax import jit

from timers import timer_decorator

class ValGrad():
    def __init__(self, val, grad):
        self.val = val
        self.grad = grad

@timer_decorator
@jit
def multigamma(a, p):
    body_func = lambda i,x: x*jnp.exp(gammaln(a + 0.5*(1-i)))
    b = lax.fori_loop(1, p+1, body_func, 1.)
    val = jnp.power(jnp.pi, 0.25*p*(p-1)) * b

    return val

@timer_decorator
@jit
def log_inv_gamma_pdf(x, a, b):
    # reshapes on x to (-1,1)?
    y = a*jnp.log(b) - gammaln(a) - (a+1)*jnp.log(x) - b/x
    return y
