In order to get things going, you need to run `module load python3` and 
do `source ./bin/activate` in order to enter the virtual environment with access 
to all the required packages.

In order to install the required modules should something go wrong here or 
you're trying to run this on a different system, enter the virtual environment 
as specified above and then run `pip install -r requirements.txt`.
