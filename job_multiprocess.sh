#!/bin/bash -l

# Maximum compute quantities


# SLURM SUBMIT SCRIPT
#SBATCH --job-name=runv0
#SBATCH --nodes=1
##SBATCH --gres=gpu:1
#SBATCH --tasks-per-node=1
##SBATCH --cpus-per-task=1
#SBATCH --mem=2g
#SBATCH --time=0-05:00:00
##SBATCH --signal=USR1@300
#SBATCH --mail-type=END,FAIL


# Activate virtual environment

#module load cuda
#module load cudnn

#conda activate python123# Activate the environment to run the code
source /scratch/jz21/gs1342/src/cvvb_check/bin/activate

_SAVEDIR='/scratch/jz21/gs1342/src/results/' # Your result save directory

# Train the model
python3 /scratch/jz21/gs1342/src/cvvb.py \
--result_dir ${_SAVEDIR}